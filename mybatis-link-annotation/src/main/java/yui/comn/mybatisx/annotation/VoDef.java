/**
 * Project: yui3-common-mybatisx
 * Class DefVo
 * Version 1.0
 * File Created at 2019年3月25日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * dto默认vo
 * </p>
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface VoDef {
    
}
