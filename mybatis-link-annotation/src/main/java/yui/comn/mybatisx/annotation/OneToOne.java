package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import yui.comn.mybatisx.annotation.model.Clazz;
import yui.comn.mybatisx.annotation.model.JoinType;

/**
 * <p>
 * 一对一 注解配置类
 * <p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OneToOne {
    
    /**
     * <p>
     * 连表查询左表对应实体类，选填，默认就是当前Dao中的对象
     * </p>
     */
    Class<?> leftClass() default Clazz.class;
    
    /**
     * <p>
     * 连表查询右表对应实体类，必填
     * </p>
     */
    Class<?> rightClass();
    
    /**
     * <p>
     * 连表查询左对象别名，选填，默认当前leftClass类对象名
     * </p>
     */
    String leftAlias() default "";
    
    /**
     * <p>
     * 连表查询右对象别名，选填，默认当前rightClass类对象名
     * </p>
     */
    String rightAlias() default "";
    
    /**
     * <p>
     * 连表查询方式，选填默认为内连接查询
     * </p>
     * {@link JoinType}
     */
    JoinType joinType() default JoinType.INNER;
    
    /**
     * <p>
     * 左表连接字段,选填，默认就是leftClass主键
     * </p>
     */
    String leftColumn() default "";
    
    /**
     * <p>
     * 右表连接字段,选填，默认就是leftClass主键
     * </p>
     */
    String rightColumn() default "";
    
    /**
     * <p>
     * 如果是左连接或者右连接，on中需要的传参参数名称
     * </p>
     */
    String onArgName() default "";
    
}
