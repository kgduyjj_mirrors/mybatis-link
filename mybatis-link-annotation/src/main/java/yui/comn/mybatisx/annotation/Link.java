package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 连接参数
 * <p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Link {
    
    /**
     * <p>
     * 映射result map id
     * </p>
     */
    String resultMapId() default "";
    /**
     * <p>
     * 一对一集合
     * </p>
     */
    OneToOne[] ones() default {};
    /**
     * <p>
     * 一对多集合
     * </p>
     */
    OneToMany[] manys() default {};
    
    /**
     * <p>
     * 是否打印sql日志
     * </p>
     */
    boolean print() default false;
    
    /**
     * <p>
     * 是否打印resultMap日志
     * </p>
     */
    boolean printRm() default false;
    
    /**
     * <p>
     * 是否打印缓存日志
     * </p>
     */
    boolean printCache() default false;
    
    /**
     * <p>
     * 是否开启连表缓存解析，默认开启
     * </p>
     */
    boolean openCache() default true;
    
}
