/**
* Project: yui3-common-mybatisx
 * Class JoinType
 * Version 1.0
 * File Created at 2019年1月8日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.annotation.model;

import lombok.Getter;

/**
 * <p>
 * 连表查询类型
 * <p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Getter
public enum JoinType {
    /**
     * 无连接，抛弃左边
     */
    NONE(" "),
    /**
     * 内连接
     */
    // INNER(" INNER JOIN "),
    INNER(" , "),
    /**
     * 左连接
     */
    LEFT(" LEFT JOIN "),
    /**
     * 右连接
     */
    RIGHT(" RIGHT JOIN "),
    ;

    private String key;

    JoinType(String key) {
        this.key = key;
    }



}
