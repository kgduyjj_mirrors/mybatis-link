/**
* Project: yui3-common-mybatisx
 * Class Clazz
 * Version 1.0
 * File Created at 2019年1月9日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.annotation.model;

/**
 * <p>
 * 初始类,作为@interface注解的默认类，没有实际含义
 * <p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
public class Clazz {

}
