/**
 * Project: mybatis-link-annotation
 * Class RelDao
 * Version 1.0
 * File Created at 2021-4-21
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import yui.comn.mybatisx.annotation.model.Clazz;

/**
 * <p>
 * 关联的Dao 
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DaoDef {

    Class<?> value() default Clazz.class;
    
}
