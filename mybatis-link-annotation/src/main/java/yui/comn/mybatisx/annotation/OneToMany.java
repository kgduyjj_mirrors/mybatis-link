package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import yui.comn.mybatisx.annotation.model.Clazz;

/**
 * <p>
 * 一对多 注解配置类
 * <p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OneToMany {
    
    /**
     * <p>
     * 连表查询左表对应实体类，选填，默认就是当前Dao中的对象
     * </p>
     */
    Class<?> leftClass() default Clazz.class;
    
    /**
     * <p>
     * 连表查询左对象别名，选填，默认当前leftClass类对象名
     * </p>
     */
    String leftAlias() default "";
    
    /**
     * <p>
     * 一对多，一中的连接字段,选填，默认就是leftClass主键
     * </p>
     */
    String leftColumn() default "";
    
    /**
     * <p>
     * 一对多，多的实体类, 必填
     * </p>
     */
    Class<?> ofTypeClass();
    /**
     * <p>
     * 一对多，多的实体类,选填, 默认通过ofTypeClass来获取rightClass类
     * </p>
     */
    Class<?> rightClass() default Clazz.class;
    
    /**
     * <p>
     * 一对多，多中查询列表别名，选填，默认当前rightClass类对象名
     * </p>
     */
    String rightAlias() default "";
    
    /**
     * <p>
     * 一对多，多中的连接字段,选填，默认就是rightClass主键
     * </p>
     */
    String rightColumn() default "";
    
    /**
     * <p>
     * 一对多，多的实体对象名称
     * </p>
     */
    String property();
    
    /**
     * <p>
     * 一对多，多中可以进行一对一集合
     * </p>
     */
    OneToOne[] ones() default {};

}
