/**
 * Project: yui3-common-mybatisx
 * Class TenantId
 * Version 1.0
 * File Created at 2019年3月25日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 租户ID
 * </p>
 * @author yuyi (1060771195@qq.com)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TenantId {

    
    // 不要在这个注解设置数据库字段名称，因为是扩展，解析的时候不起作用，还是需要用@TableField该注解
    
//    /**
//     * <p>
//     * 字段值（驼峰命名方式，该值可无）
//     * </p>
//     */
//    String value() default "";
    
    
}
