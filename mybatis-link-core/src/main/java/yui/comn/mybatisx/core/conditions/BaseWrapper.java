/**
 * Project: yui3-common-mybatisx-base
 * Class BaseWrapper
 * Version 1.0
 * File Created at 2020-12-6
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.conditions;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 条件构造抽象基础类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public abstract class BaseWrapper implements Serializable {
    private static final long serialVersionUID = -8828722489920368354L;

    /**
     * 左连接或者右连接  ON 数据库关键字 后面可能需要的参数 
     */
    public Map<String, String> sqlOnMap;
    
    /**
     * 主键
     */
    public Serializable id;
    
    /**
     * 租户主键
     */
    public Serializable tntId;
    
    /**
     * 表名
     */
    public Serializable tableName;
    
    /**
     * 连表查询动态表名 
     */
    public Map<String, String> tableNameMap;
    
    /**
     * 最大条数
     */
    public int maxRow = 1000;
    
    /**
     * 当前页码
     */
    public Integer pageNo = 1;
    
    /**
     * 每页条数
     */
    public Integer pageSize = 10;
    
    /**
     * 是否查询条数
     */
    boolean isSearchCount = true;
    
    /**
     * 继承 IComnSeqment 接口
     */
    public Serializable getId() {
        return id;
    }

    public Serializable getTntId() {
        return tntId;
    }
    
    public Serializable getTableName() {
        return tableName;
    }


    public Map<String, String> getTableNameMap() {
        return tableNameMap;
    }

    public Map<String, String> getSqlOnMap() {
        return sqlOnMap;
    }
    
    public void addSqlOn(String key, String val) {
        if (null == sqlOnMap) {
            sqlOnMap = new HashMap<>();
        }
        sqlOnMap.put(key, val);
    }
    
    public void setSqlOnMap(Map<String, String> sqlOnMap) {
        this.sqlOnMap = sqlOnMap;
    }

    public void setId(Serializable id) {
        this.id = id;
    }

    public void setTntId(Serializable tntId) {
        this.tntId = tntId;
    }
    
    public void setTableName(Serializable tableName) {
        this.tableName = tableName;
    }
    
    public void addTableName(String key, String val) {
        if (null == tableNameMap) {
            tableNameMap = new HashMap<>();
            setTableName(val); // 兼容单表查询动态表名
        }
        tableNameMap.put(key, val);
    }
    
    public void setTableNameMap(Map<String, String> tableNameMap) {
        this.tableNameMap = tableNameMap;
    }

    public int getMaxRow() {
        return maxRow;
    }

    public void setMaxRow(int maxRow) {
        this.maxRow = maxRow;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isSearchCount() {
        return isSearchCount;
    }

    public void setSearchCount(boolean isSearchCount) {
        this.isSearchCount = isSearchCount;
    }
    
}
