/**
 * Project: mybatis-link-core
 * Class InjectorMapperCacheAssistant
 * Version 1.0
 * File Created at 2021-4-19
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;

import yui.comn.mybatisx.annotation.Link;
import yui.comn.mybatisx.annotation.OneToMany;
import yui.comn.mybatisx.annotation.OneToOne;
import yui.comn.mybatisx.annotation.model.Clazz;
import yui.comn.mybatisx.annotation.model.JoinType;
import yui.comn.mybatisx.core.exceptions.MybatisLinkException;
import yui.comn.mybatisx.core.toolkit.ClazzUtils;

/**
 * <p>
 * 连表查询缓存解析
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class InjectorCacheAssistant {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private final Class<?> mapperClass;
    private final Class<?> modelTypeClass;
    
    public InjectorCacheAssistant(Class<?> mapperClass) {
        this.mapperClass = mapperClass;
        this.modelTypeClass = extractModelClass(mapperClass);
    }
    
    public void parse(Link link, Method method) {
    	if (!link.openCache()) {
    		return;
    	}
    	
        if (link.printCache()) {
            logger.info(method.getName() + "--cache");
        }
        
        parseOne(link);
        
        parseMany(link);
    }
    
    public void parseOne(Link link) {
        parseOne(link.ones());
    }
    
    public void parseOne(OneToOne[] ones) {
        if (ArrayUtils.isNotEmpty(ones)) {
            for (OneToOne one : ones) {
                Class<?> leftClass = one.leftClass() != Clazz.class ? one.leftClass() : modelTypeClass;
                Class<?> rightClass = one.rightClass();
                
                CacheMapping.put(mapperClass, modelTypeClass, leftClass);
                CacheMapping.put(mapperClass, modelTypeClass, rightClass);
            }
        }
    }
    
    public void parseMany(Link link) {
        OneToMany[] manys = link.manys();
        if (ArrayUtils.isNotEmpty(manys)) {
            for (OneToMany many : manys) {
                Class<?> rightClass = many.rightClass();
                if (rightClass == Clazz.class) {
                    rightClass = extractModelClassFromOfTypeClass(many.ofTypeClass());
                    if (null == rightClass) {
                        throw new MybatisLinkException("OneToMany 没有对应的  实体类");
                    }
                }
                OneToOne[] ones2 = many.ones();
                OneToOne[] ones = new OneToOne[ones2.length + 1];
                ones[0] = getOneFromMany(many, link);
                if (ArrayUtils.isNotEmpty(ones2)) {
                    for (int i = 0; i < ones2.length; i++) {
                        ones[i+1] = ones2[i];
                    }
                }
                
                parseOne(ones);
            }
        }
    }
    
    private OneToOne getOneFromMany(OneToMany many, Link link) {
        final Class<?> leftClass = many.leftClass() != Clazz.class ? many.leftClass() : modelTypeClass;
        
        return new OneToOne() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return OneToOne.class;
            }
            
            @Override
            public String rightColumn() {
                return many.rightColumn();
            }
            
            @Override
            public Class<?> rightClass() {
                if (many.rightClass() != Clazz.class) {
                    return many.rightClass();
                } else {
                    return extractModelClassFromOfTypeClass(many.ofTypeClass());
                }
            }
            
            @Override
            public String rightAlias() {
                return null;
            }
            
            @Override
            public String onArgName() {
                return null;
            }
            
            @Override
            public String leftColumn() {
                return many.leftColumn();
            }
            
            @Override
            public Class<?> leftClass() {
                return leftClass;
            }
            
            @Override
            public String leftAlias() {
                return many.leftAlias();
            }
            
            @Override
            public JoinType joinType() {
                return JoinType.NONE;
            }
        };
    }
    
    public Class<?> extractModelClassFromOfTypeClass(Class<?> ofTypeClass) {
    	return ClazzUtils.extractModelClassFromOfTypeClass(ofTypeClass);
    }
    
    public Class<?> extractModelClass(Class<?> mapperClass) {
        Type[] types = extractTypes(mapperClass);
        return types == null ? null : (Class<?>) types[0];
    }
    
    public Type[] extractTypes(Class<?> mapperClass) {
        Type[] types = mapperClass.getGenericInterfaces();
        ParameterizedType target = null;
        for (Type type : types) {
            if (type instanceof ParameterizedType) {
                Type[] typeArray = ((ParameterizedType) type).getActualTypeArguments();
                if (ArrayUtils.isNotEmpty(typeArray)) {
                    for (Type t : typeArray) {
                        if (t instanceof TypeVariable || t instanceof WildcardType) {
                            break;
                        } else {
                            target = (ParameterizedType) type;
                            break;
                        }
                    }
                }
                break;
            }
        }
        return target == null ? null : target.getActualTypeArguments();
    }
}
