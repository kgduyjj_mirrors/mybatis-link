package yui.comn.mybatisx.core.conditions.segments;


import static com.baomidou.mybatisplus.core.enums.SqlKeyword.HAVING;
import static java.util.stream.Collectors.joining;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;

/**
 * <p>
 * Having SQL 片段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class HavingSegmentList extends AbstractISegmentList {
    private static final long serialVersionUID = 2274849095559606636L;

    @Override
    protected boolean transformList(List<ISqlSegment> list, ISqlSegment firstSegment, ISqlSegment lastSegment) {
        if (!isEmpty()) {
            this.add(SqlKeyword.AND);
        }
        list.remove(0);
        return true;
    }

    @Override
    protected String childrenSqlSegment() {
        if (isEmpty()) {
            return EMPTY;
        }
        return this.segmentList.stream().map(ISqlSegment::getSqlSegment).collect(joining(SPACE, SPACE + HAVING.getSqlSegment() + SPACE, EMPTY));
    }
}
