package yui.comn.mybatisx.core.toolkit;

import java.lang.reflect.Field;
import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;

import yui.comn.mybatisx.annotation.VoDef;

public class ClazzUtils {

	public static Class<?> extractModelClassFromOfTypeClass(Class<?> ofTypeClass) {
        List<Field> fieldList = ReflectionKit.getFieldList(ofTypeClass);
        for (Field field : fieldList) {
            VoDef anno = field.getAnnotation(VoDef.class);
            if (null != anno) {
                return field.getType();
            }
        }
        
        String simpleName = ofTypeClass.getSimpleName();
        String voName = simpleName.substring(0, simpleName.length() - 3) + "Vo";
        
        Field[] fields = ofTypeClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.getName().equalsIgnoreCase(voName)) {
                return field.getType();
            }
        }
        return null;
	}
	
}
