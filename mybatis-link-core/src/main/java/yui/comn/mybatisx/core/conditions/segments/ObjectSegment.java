/**
* Project: yui3-common-mybatisx
 * Class ObjectSegment
 * Version 1.0
 * File Created at 2019年1月22日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.conditions.segments;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;

/**
 * sqlSegment入参类
 * @author yuyi (1060771195@qq.com)
 */
public class ObjectSegment implements ISqlSegment{
    private static final long serialVersionUID = -2868821656119295316L;

    private Object arg$1;
    private Object arg$2;
    
    public ObjectSegment(Object arg$2) {
        this.arg$2 = arg$2;
    }
    
    public ObjectSegment(Object arg$1, Object arg$2) {
        this.arg$1 = arg$1;
        this.arg$2 = arg$2;
    }

    public Object getArg$1() {
        return arg$1;
    }

    public void setArg$1(Object arg$1) {
        this.arg$1 = arg$1;
    }

    public Object getArg$2() {
        return arg$2;
    }

    public void setArg$2(Object arg$2) {
        this.arg$2 = arg$2;
    }

    @Override
    public String getSqlSegment() {
        return String.valueOf(arg$2);
    }
    
}
