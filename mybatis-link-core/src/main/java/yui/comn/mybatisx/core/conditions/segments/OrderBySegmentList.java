package yui.comn.mybatisx.core.conditions.segments;


import static com.baomidou.mybatisplus.core.enums.SqlKeyword.ORDER_BY;
import static java.util.stream.Collectors.joining;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;

/**
 * <p>
 * Order By SQL 片段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class OrderBySegmentList extends AbstractISegmentList {
    private static final long serialVersionUID = 4075051666006811554L;

    @Override
    protected boolean transformList(List<ISqlSegment> list, ISqlSegment firstSegment, ISqlSegment lastSegment) {
        list.remove(0);
        final String sql = list.stream().map(ISqlSegment::getSqlSegment).collect(joining(SPACE));
        list.clear();
        // list.add(() -> sql);
        list.add(new ObjectSegment(sql));
        return true;
    }

    @Override
    protected String childrenSqlSegment() {
        if (isEmpty()) {
            return EMPTY;
        }
        return this.segmentList.stream().map(ISqlSegment::getSqlSegment).collect(joining(COMMA, SPACE + ORDER_BY.getSqlSegment() + SPACE, EMPTY));
    }
}
