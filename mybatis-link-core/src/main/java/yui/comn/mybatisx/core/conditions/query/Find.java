/**
* Project: yui3-common-mybatisx
 * Class Find
 * Version 1.0
 * File Created at 2019年1月23日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.conditions.query;

import java.io.Serializable;
import java.util.function.Predicate;

import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;

/**
 * 查询扩展类
 * @author yuyi (1060771195@qq.com)
 */
public interface Find<Children, T, R> extends Serializable { // extends Query<Children, T, R> {

    /**
     * 设置查询字段
     *
     * @param columns 字段数组
     * @return children
     */
    @SuppressWarnings("unchecked")
    Children selectOrig(R... columns);

    /**
     * ignore
     * <p>注意只有内部有 entity 才能使用该方法</p>
     */
    default Children selectOrig(Predicate<TableFieldInfo> predicate) {
        return selectOrig(null, predicate);
    }

    /**
     * 过滤查询的字段信息(主键除外!)
     * <p>例1: 只要 java 字段名以 "test" 开头的             -> select(i -> i.getProperty().startsWith("test"))</p>
     * <p>例2: 只要 java 字段属性是 CharSequence 类型的     -> select(TableFieldInfo::isCharSequence)</p>
     * <p>例3: 只要 java 字段没有填充策略的                 -> select(i -> i.getFieldFill() == FieldFill.DEFAULT)</p>
     * <p>例4: 要全部字段                                   -> select(i -> true)</p>
     * <p>例5: 只要主键字段                                 -> select(i -> false)</p>
     *
     * @param predicate 过滤方式
     * @return children
     */
    Children selectOrig(Class<T> entityClass, Predicate<TableFieldInfo> predicate);

    /**
     * 查询条件 SQL 片段
     */
    String getSqlSelect();
    
    /**
     * 设置查询字段
     *
     * @param columns 字段数组
     * @return children
     */
    @SuppressWarnings("unchecked")
    Children select(R... columns);
    
    /**
     * 设置查询字段
     *
     * @param clazz 实体类
     * @param columns 字段数组
     * @return children
     */
    @SuppressWarnings("unchecked")
    Children select(Class<?> clazz, R... columns);
    
    /**
     * 设置查询字段
     *
     * @param alias 别名
     * @param columns 字段数组
     * @return children
     */
    @SuppressWarnings("unchecked")
    Children selectAlias(String alias, R... columns);
    
    
}
