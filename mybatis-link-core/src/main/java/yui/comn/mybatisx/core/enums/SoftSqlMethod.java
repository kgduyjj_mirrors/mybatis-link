package yui.comn.mybatisx.core.enums;


/**
 * <p>
 * Mybatisx 支持 SQL 方法
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public enum SoftSqlMethod {
    
    EMPTY("empty", "", "<script>\n%s\n</script>"),
    /**
     * 插入
     */
    INSERT("insert", "插入一条数据（选择字段插入）", "<script>\nINSERT INTO %s %s VALUES %s\n</script>"),
    /**
     * 批量插入
     */
    INSERT_BATCH("insertBatch", "批量插入数据（选择字段插入）", "<script>\nINSERT INTO %s %s VALUES %s\n</script>"),
    /**
     * 修改
     */
    UPDATE_BY_ID("updateById", "根据ID 选择修改数据", "<script>\nUPDATE %s %s WHERE %s=#{%s} %s\n</script>"),
    UPDATE("update", "根据 whereEntity 条件，更新记录", "<script>\nUPDATE %s %s %s\n</script>"),
    UPDATE_ALL_BY_ID("updateAllById", "根据ID 修改数据", "<script>\nUPDATE %s %s WHERE %s=#{%s} %s\n</script>"),
    /**
     * 批量修改
     */
    UPDATE_BATCH_BY_ID("updateBatchById", "根据ID 批量选择修改数据", "UPDATE %s %s WHERE %s=#{%s} %s"),
    /**
     * 批量修改
     */
    UPDATE_ALL_BATCH_BY_ID("updateAllBatchById", "根据ID 批量修改数据", "UPDATE %s %s WHERE %s=#{%s} %s"),
    
    /**
     * 删除
     */
    DELETE_BY_ID("deleteById", "根据ID 删除一条数据", "<script>\nDELETE FROM %s WHERE %s=#{%s}\n</script>"),
    DELETE_BY_MAP("deleteByMap", "根据columnMap 条件删除记录", "<script>\nDELETE FROM %s %s\n</script>"),
    DELETE("delete", "根据 entity 条件删除记录", "<script>\nDELETE FROM %s %s\n</script>"),
    DELETE_BY_IDS("deleteByIds", "根据ID集合，批量删除数据", "<script>\nDELETE FROM %s WHERE %s IN (%s)\n</script>"),
    /**
     * 逻辑删除
     */
    LOGIC_DELETE_BY_ID("deleteById", "根据ID 逻辑删除一条数据", "<script>\nUPDATE %s %s WHERE %s=#{%s} %s\n</script>"),
    LOGIC_DELETE_BY_MAP("deleteByMap", "根据columnMap 条件逻辑删除记录", "<script>\nUPDATE %s %s %s\n</script>"),
    LOGIC_DELETE("delete", "根据 entity 条件逻辑删除记录", "<script>\nUPDATE %s %s %s\n</script>"),
    LOGIC_DELETE_BATCH_BY_IDS("deleteBatchIds", "根据ID集合，批量逻辑删除数据", "<script>\nUPDATE %s %s WHERE %s IN (%s) %s\n</script>"),
    
    /**
     * 强制硬删
     */
    DEFUNCT("defunct", "根据 entity 强制删除记录", "<script>\nDELETE FROM %s %s %s\n</script>"),
    
    /**
     * 查询
     */
    GET_BY_ID("getById", "根据ID 查询一条数据", "SELECT %s FROM %s WHERE %s=#{%s} %s"),
    GET("get", "查询满足条件一条数据", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // GET("get", "查询满足条件一条数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    GET_BY_MAP("getByMap", "根据columnMap 查询一条数据", "<script>SELECT %s FROM %s %s\n</script>"),
    LIST("list", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // LIST("list", "查询满足条件所有数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    LIST_BATCH_BY_IDS("listBatchIds", "根据ID集合，批量查询数据", "<script>SELECT %s FROM %s WHERE %s IN (%s) %s</script>"),
    LIST_BY_MAP("listByMap", "根据columnMap 查询所有数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    LIST_MAPS("listMaps", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // LIST_MAPS("listMaps", "查询满足条件所有数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    LIST_MAPS_BY_MAP("listMapsByMap", "查询满足条件所有数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    LIST_OBJS("listObjs", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // LIST_OBJS("listObjs", "查询满足条件所有数据", "<script>\nSELECT %s FROM %s %s\n</script>"),
    COUNT("count", "查询满足条件总记录数", "<script>%s SELECT COUNT(%s) FROM %s %s %s\n</script>"),
    // COUNT("count", "查询满足条件总记录数", "<script>\nSELECT COUNT(1) FROM %s %s\n</script>"),
    PAGE("page", "查询满足条件所有数据（并翻页）", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // PAGE("page", "查询满足条件所有数据（并翻页）", "<script>\nSELECT %s FROM %s %s\n</script>"),
    PAGE_MAPS("pageMaps", "查询满足条件所有数据（并翻页）", "<script>\n %s SELECT %s FROM %s %s %s\n</script>"),
    // PAGE_MAPS("pageMaps", "查询满足条件所有数据（并翻页）", "<script>\nSELECT %s FROM %s %s\n</script>"),

    
    DYNAMIC_LIST("", "动态生成查询语句", "<script>%s SELECT %s FROM %s %s %s\n</script>"),
    // DYNAMIC_LIST("", "动态生成查询语句", "<script>\nSELECT %s FROM %s %s\n</script>"),
    DYNAMIC_COUNT("", "动态生成查询语句", "<script>%s SELECT COUNT(%s) FROM %s %s %s\n</script>"),
    // DYNAMIC_COUNT("", "动态生成查询语句", "<script>\nSELECT COUNT(1) FROM %s %s\n</script>"),
    ;

    private final String method;
    private final String desc;
    private final String sql;

    SoftSqlMethod(String method, String desc, String sql) {
        this.method = method;
        this.desc = desc;
        this.sql = sql;
    }

    public String getMethod() {
        return method;
    }

    public String getDesc() {
        return desc;
    }

    public String getSql() {
        return sql;
    }

}
