/**
 * Project: yui3-common-mybatisx
 * Class IComnSeqment
 * Version 1.0
 * File Created at 2019年3月25日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.conditions;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 公共字段片段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public interface IComnSeqment extends Serializable {
    
    /**
     * 左连接或者右连接  ON 数据库关键字 后面可能需要的参数 
     */
    Map<String, String> getSqlOnMap();
    
    /**
     * 主键
     */
    Serializable getId();
    
    /**
     * 租户
     */
    Serializable getTntId();
    
    /**
     * 表名
     */
    Serializable getTableName();
    
    /**
     * 连表查询动态表名 一般用于数据量比较大的表 
     * tableNameMap.put(t_test_e, t_test_e_ro)
     */
    Map<String, String> getTableNameMap();
}
