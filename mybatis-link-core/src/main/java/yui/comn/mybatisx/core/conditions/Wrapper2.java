//package yui.comn.mybatisx.core.conditions;
//
//import java.io.Serializable;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Objects;
//
//import com.baomidou.mybatisplus.core.conditions.ISqlSegment;
//import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
//import com.baomidou.mybatisplus.core.metadata.TableInfo;
//import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
//import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
//import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
//import com.baomidou.mybatisplus.core.toolkit.StringUtils;
//
//import yui.comn.mybatisx.core.conditions.segments.MergeSegments;
//
///**
// * <p>
// * 条件构造抽象类
// * </p>
// *
// * @author yuyi (1060771195@qq.com)
// */
//public abstract class Wrapper2<T> implements ISqlSegment, IComnSeqment {
//    private static final long serialVersionUID = -1401454060435332540L;
//
//    /**
//     * 左连接或者右连接  ON 数据库关键字 后面可能需要的参数 
//     */
//    public Map<String, String> sqlOnMap;
//    
//    /**
//     * 主键
//     */
//    public Serializable id;
//    
//    /**
//     * 租户主键
//     */
//    public Serializable tntId;
//    
//    /**
//     * 表名
//     */
//    public Serializable tableName;
//    
//    /**
//     * 连表查询动态表名 
//     */
//    public Map<String, String> tableNameMap;
//    
//    /**
//     * 最大条数
//     */
//    public int maxRow = 1000;
//    
//    /**
//     * 当前页码
//     */
//    public Integer pageNo = 1;
//    
//    /**
//     * 每页条数
//     */
//    public Integer pageSize = 10;
//    
//    /**
//     * 是否查询条数
//     */
//    boolean isSearchCount = true;
//    
//    /**
//     * <p>
//     * 实体对象（子类实现）
//     * </p>
//     *
//     * @return 泛型 T
//     */
//    public abstract T getEntity();
//
//    
//    public String getSqlSelect() {
//        return null;
//    }
//
//    public String getSqlSet() {
//        return null;
//    }
//    
//
//    /**
//     * 继承 IComnSeqment 接口
//     */
//    public Serializable getId() {
//        return id;
//    }
//
//    public Serializable getTntId() {
//        return tntId;
//    }
//    
//    public Serializable getTableName() {
//        return tableName;
//    }
//
//
//    public Map<String, String> getTableNameMap() {
//        return tableNameMap;
//    }
//
//    public Map<String, String> getSqlOnMap() {
//        return sqlOnMap;
//    }
//
//    /**
//     * 获取 MergeSegments
//     */
//    public abstract MergeSegments getExpression();
//
//    /**
//     * 获取自定义SQL 简化自定义XML复杂情况
//     * 使用方法
//     * `自定义sql` + ${ew.customSqlSegment}
//     * <p>1.逻辑删除需要自己拼接条件 (之前自定义也同样)</p>
//     * <p>2.不支持wrapper中附带实体的情况 (wrapper自带实体会更麻烦)</p>
//     * <p>3.用法 ${ew.customSqlSegment} (不需要where标签包裹,切记!)</>
//     * <p>4.ew是wrapper定义别名,可自行替换</>
//     */
//    public abstract String getCustomSqlSegment();
//
//    
//    /**
//     * 查询条件为空(包含entity)
//     */
//    public boolean isEmptyOfWhere() {
//        return isEmptyOfNormal() && isEmptyOfEntity();
//    }
//
//    /**
//     * 查询条件不为空(包含entity)
//     */
//    public boolean nonEmptyOfWhere() {
//        return !isEmptyOfWhere();
//    }
//
//    /**
//     * 查询条件为空(不包含entity)
//     */
//    public boolean isEmptyOfNormal() {
//        return CollectionUtils.isEmpty(getExpression().getNormal().getSegmentList());
//    }
//
//    /**
//     * 查询条件为空(不包含entity)
//     */
//    public boolean nonEmptyOfNormal() {
//        return !isEmptyOfNormal();
//    }
//
//    /**
//     * 深层实体判断属性
//     *
//     * @return true 不为空
//     */
//    public boolean nonEmptyOfEntity() {
//        T entity = getEntity();
//        if (entity == null) {
//            return false;
//        }
//        TableInfo tableInfo = TableInfoHelper.getTableInfo(entity.getClass());
//        if (tableInfo == null) {
//            return false;
//        }
//        if (tableInfo.getFieldList().stream().anyMatch(e -> fieldStrategyMatch(entity, e))) {
//            return true;
//        }
//        return StringUtils.isNotEmpty(tableInfo.getKeyProperty()) ? Objects.nonNull(ReflectionKit.getMethodValue(entity, tableInfo.getKeyProperty())) : false;
//    }
//
//    /**
//     * 根据实体FieldStrategy属性来决定判断逻辑
//     */
//    private boolean fieldStrategyMatch(T entity, TableFieldInfo e) {
//        switch (e.getFieldStrategy()) {
//            case NOT_NULL:
//                return Objects.nonNull(ReflectionKit.getMethodValue(entity, e.getProperty()));
//            case IGNORED:
//                return true;
//            case NOT_EMPTY:
//                return StringUtils.checkValNotNull(ReflectionKit.getMethodValue(entity, e.getProperty()));
//            default:
//                return Objects.nonNull(ReflectionKit.getMethodValue(entity, e.getProperty()));
//        }
//    }
//
//    /**
//     * 深层实体判断属性
//     *
//     * @return true 为空
//     */
//    public boolean isEmptyOfEntity() {
//        return !nonEmptyOfEntity();
//    }
//    
//    public void addSqlOn(String key, String val) {
//        if (null == sqlOnMap) {
//            sqlOnMap = new HashMap<>();
//        }
//        sqlOnMap.put(key, val);
//    }
//    
//    public void setSqlOnMap(Map<String, String> sqlOnMap) {
//        this.sqlOnMap = sqlOnMap;
//    }
//
//    public void setId(Serializable id) {
//        this.id = id;
//    }
//
//    public void setTntId(Serializable tntId) {
//        this.tntId = tntId;
//    }
//    
//    public void setTableName(Serializable tableName) {
//        this.tableName = tableName;
//    }
//    
//    public void addTableName(String key, String val) {
//        if (null == tableNameMap) {
//            tableNameMap = new HashMap<>();
//            setTableName(val); // 兼容单表查询动态表名
//        }
//        tableNameMap.put(key, val);
//    }
//    
//    public void setTableNameMap(Map<String, String> tableNameMap) {
//        this.tableNameMap = tableNameMap;
//    }
//
//    public int getMaxRow() {
//        return maxRow;
//    }
//
//    public void setMaxRow(int maxRow) {
//        this.maxRow = maxRow;
//    }
//
//    public Integer getPageNo() {
//        return pageNo;
//    }
//
//    public void setPageNo(Integer pageNo) {
//        this.pageNo = pageNo;
//    }
//
//    public Integer getPageSize() {
//        return pageSize;
//    }
//
//    public void setPageSize(Integer pageSize) {
//        this.pageSize = pageSize;
//    }
//
//    public boolean isSearchCount() {
//        return isSearchCount;
//    }
//
//    public void setSearchCount(boolean isSearchCount) {
//        this.isSearchCount = isSearchCount;
//    }
//    
//}
