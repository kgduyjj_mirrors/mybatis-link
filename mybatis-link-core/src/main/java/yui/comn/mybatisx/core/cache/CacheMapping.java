/**
 * Project: mybatis-link-core
 * Class CacheMapping
 * Version 1.0
 * File Created at 2021-4-18
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import yui.comn.mybatisx.annotation.DaoDef;
import yui.comn.mybatisx.annotation.model.Clazz;

/**
 * <p>
 * 缓存映射
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@SuppressWarnings("rawtypes")
public class CacheMapping {

    public static final Map<Class, Class> VO_MAPPING_DAO_MAP = new HashMap<>();
    public static final Map<String, Set<String>> DAO_MAPPING_DAOS_MAP = new HashMap<>();
    
	public static void put(Class<?> daoClass, Class<?> modelClass, Class<?> linkModelClass) {
        if (!match(modelClass, linkModelClass)) {
        	Class linkMapperClass = VO_MAPPING_DAO_MAP.get(linkModelClass);
        	if (null == linkMapperClass) {
        		linkMapperClass = extractMapperClassFromEntityClass(linkModelClass);
        		if (null != linkMapperClass) {
        			VO_MAPPING_DAO_MAP.put(linkModelClass, linkMapperClass);
        		}
        	}
        	
            if (null != linkMapperClass) {
                putDao(linkMapperClass.getName(), daoClass.getName());
            }
        }
    }
    
    public static void putDao(String daoKey, String daoVal) {
        Set<String> set = DAO_MAPPING_DAOS_MAP.get(daoKey);
        if (null == set) {
            set = new HashSet<>();
            DAO_MAPPING_DAOS_MAP.put(daoKey, set);
        }
        if (!set.contains(daoVal)) {
        	set.add(daoVal);
        }
    }
    
    public static boolean match(Class<?> modelClass, Class<?> modelClass1) {
        return modelClass == modelClass1;
    }
    
    public static Set<String> get(String key) {
        return DAO_MAPPING_DAOS_MAP.get(key);
    }
    
    public static Class<?> extractMapperClassFromEntityClass(Class<?> entityClass) {
        DaoDef daoDef = entityClass.getAnnotation(DaoDef.class);
        
        if (null != daoDef) {
            Class<?> cls = daoDef.value();
            if (null != cls && cls != Clazz.class) {
                return cls;
            }
        }
        
        String entityName = entityClass.getName();
        String daoName = entityName.replace(".vo.", ".dao.")
        		.substring(0, entityName.length() - 1) + "Dao";
        
        try {
			return Class.forName(daoName);
		} catch (ClassNotFoundException e) {
		}
        return null;
    }
    
}
