/**
 * Project: yui3-common-mybatisx
 * Class SoftConstants
 * Version 1.0
 * File Created at 2020年3月13日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit;

import com.baomidou.mybatisplus.core.toolkit.Constants;

/**
 * <p>
 * mybatis_plus_x 自用常量集中管理
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public interface SoftConstants extends Constants {

    /**
     * collection
     */
    String COLL = "collection";
    /**
     * collection
     */
    String INDEX = "index";
    /**
     * collection
     */
    String ITEM = "item";
    /**
     * collection
     */
    String ITEM_DOT = ITEM + DOT;
}
