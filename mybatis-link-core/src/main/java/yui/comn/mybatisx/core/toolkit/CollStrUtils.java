/**
 * Project: yui3-common-mybatisx-base
 * Class CollStrUtils
 * Version 1.0
 * File Created at 2020-10-11
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

/**
 * <p>
 * 字符串处理类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class CollStrUtils {

    @SuppressWarnings("unchecked")
    public static List<String> toStrList(Object args) {
        if (null == args) {
            return null;
        }
        
        if (args instanceof List<?>) {
            return (List<String>) args;
        } else if (args instanceof String) {
            if (StringUtils.checkValNull(args)) {
                return null;
            }
            
            // String[] strs = StringUtils.split((String) args, StringPool.COMMA);
            String[] strs = ((String) args).split(StringPool.COMMA);
            List<String> strList = new ArrayList<String>();
            for (String str : strs) {
                if (StringUtils.checkValNotNull(str)) {
                    strList.add(str.trim());
                }
            }
            return strList;
        }
        
        return null;
    }
}
