/**
 * Project: yui3-common-mybatisx
 * Class PageUtils
 * Version 1.0
 * File Created at 2019年5月7日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import yui.comn.mybatisx.core.conditions.Wrapper;

/**
 * <p>
 * 分页获取器
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class PageUtils {

    /**
     * {@link EntityUtils}
     * @param <D>
     * @param <V>
     * @param wrapper
     * @return
     */
    @Deprecated
    public static <D, V> IPage<D> getPage(Wrapper<V> wrapper) {
        if (null == wrapper) {
            return new Page<D>(1, 10);
        }
        return new Page<D>(wrapper.getPageNo(), wrapper.getPageSize(), 
                wrapper.isSearchCount());
    }
    
}
