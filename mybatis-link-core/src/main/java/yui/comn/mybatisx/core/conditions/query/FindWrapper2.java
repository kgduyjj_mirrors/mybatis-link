//package yui.comn.mybatisx.core.conditions.query;
//
//import java.util.Map;
//import java.util.concurrent.atomic.AtomicInteger;
//
//import com.baomidou.mybatisplus.core.conditions.SharedString;
//import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
//import com.baomidou.mybatisplus.core.toolkit.Constants;
//import com.baomidou.mybatisplus.core.toolkit.StringPool;
//import com.baomidou.mybatisplus.core.toolkit.StringUtils;
//
//import yui.comn.mybatisx.core.conditions.AbstractWrapper;
//import yui.comn.mybatisx.core.conditions.segments.MergeSegments;
//import yui.comn.mybatisx.core.toolkit.TableUtils;
//
///**
// * <p>
// * Entity 对象封装操作类
// * </p>
// *
// * @author yuyi (1060771195@qq.com)
// */
//public class FindWrapper2<T> extends AbstractWrapper<T, String, FindWrapper2<T>> implements Find<FindWrapper2<T>, T, String> {
//    private static final long serialVersionUID = -6626186064689261930L;
//    
//    public static <Y> FindWrapper2<Y> get(FindWrapper2<Y> wrapper) {
//        if (null == wrapper) {
//            return new FindWrapper2<>();
//        }
//        return wrapper;
//    }
//    
//    /**
//     * 查询字段
//     */
//    private SharedString sqlSelect = new SharedString();
//    
//    public FindWrapper2() {
//        // this(null);
//        super.initNeed();
//    }
//    
//    public FindWrapper2(Long id) {
//        setId(id);
//        super.initNeed();
//    }
//    
//    public FindWrapper2(Class<T> entityClass) {
//        super.entityClass = entityClass;
//        super.initNeed();
//    }
//
//    public FindWrapper2(T entity) {
//        super.setEntity(entity);
//        super.initNeed();
//    }
//
//    public FindWrapper2(T entity, String... columns) {
//        super.setEntity(entity);
//        super.initNeed();
//        this.select(columns);
//    }
//
//    /**
//     * 非对外公开的构造方法,只用于生产嵌套 sql
//     *
//     * @param entityClass 本不应该需要的
//     */
//    private FindWrapper2(T entity, Class<T> entityClass, AtomicInteger paramNameSeq,
//                         Map<String, Object> paramNameValuePairs, MergeSegments mergeSegments) {
//        super.setEntity(entity);
//        this.entityClass = entityClass;
//        this.paramNameSeq = paramNameSeq;
//        this.paramNameValuePairs = paramNameValuePairs;
//        this.expression = mergeSegments;
//    }
//
//    @Override
//    public FindWrapper2<T> selectOrig(String... columns) {
//        if (ArrayUtils.isNotEmpty(columns)) {
//            this.sqlSelect.setStringValue(String.join(StringPool.COMMA, columns));
//        }
//        return typedThis;
//    }
//    
//    /**
//     * <p>
//     * 过滤字段
//     * 比如："t_sys_user.username", "t_sys_user.password"
//     * </p>
//     * 
//     */
//    @Override
//    public FindWrapper2<T> select(String... columns) {
//        return select(entityClass, columns);
//    }
//    
//    /**
//     * <p>
//     * 过滤字段
//     * 比如：clazz = SysUserVo.class, columns="username", "password"
//     * </p>
//     * 
//     */
//    @Override
//    public FindWrapper2<T> select(Class<?> clazz, String... columns) {
//        if (ArrayUtils.isNotEmpty(columns)) {
//            if (null == clazz) {
//                clazz = entityClass;
//            }
//            if (StringUtils.isEmpty(this.sqlSelect.getStringValue())) {
//                this.sqlSelect.setStringValue(TableUtils.sqlSelect(clazz, columns));
//            } else {
//                this.sqlSelect.setStringValue(this.sqlSelect.getStringValue() 
//                        + Constants.COMMA + TableUtils.sqlSelect(clazz, columns));
//            }
//        }
//        return typedThis;
//    }
//    
//    /**
//     * <p>
//     * 过滤字段
//     * 比如：alias="t_sys_user", columns="username", "password"
//     * </p>
//     * 
//     */
//    @Override
//    public FindWrapper2<T> selectAlias(String alias, String... columns) {
//        if (ArrayUtils.isNotEmpty(columns)) {
//            if (StringUtils.isEmpty(this.sqlSelect.getStringValue())) {
//                this.sqlSelect.setStringValue(TableUtils.sqlSelect(alias, columns)); 
//            } else {
//                this.sqlSelect.setStringValue(this.sqlSelect.getStringValue() 
//                        + Constants.COMMA + TableUtils.sqlSelect(alias, columns));
//            }
//        }
//        return typedThis;
//    }
//    
//    
//    
////    @Override
////    public FindWrapper<T> select(String... columns) {
////        if (ArrayUtils.isNotEmpty(columns)) {
////            this.sqlSelect.setStringValue(String.join(StringPool.COMMA, columns));
////        }
////        return typedThis;
////    }
////    
////    
////    @Override
////    public FindWrapper<T> select(Predicate<TableFieldInfo> predicate) {
////        return select(entityClass, predicate);
////    }
////
////    @Override
////    public FindWrapper<T> select(Class<T> entityClass, Predicate<TableFieldInfo> predicate) {
////        this.entityClass = entityClass;
////        this.sqlSelect.setStringValue(TableInfoHelper.getTableInfo(getCheckEntityClass()).chooseSelect(predicate));
////        return typedThis;
////    }
//
//    @Override
//    public String getSqlSelect() {
//        return sqlSelect.getStringValue();
//    }
//
//    /**
//     * <p>
//     * 用于生成嵌套 sql
//     * 故 sqlSelect 不向下传递
//     * </p>
//     */
//    @Override
//    protected FindWrapper2<T> instance() {
//        return new FindWrapper2<>(entity, entityClass, paramNameSeq, paramNameValuePairs, new MergeSegments());
//    }
//
//}
