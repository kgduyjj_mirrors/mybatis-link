package yui.comn.mybatisx.core.conditions.query;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import yui.comn.mybatisx.core.conditions.AbstractWrapper;
import yui.comn.mybatisx.core.conditions.SharedString;
import yui.comn.mybatisx.core.conditions.segments.MergeSegments;
import yui.comn.mybatisx.core.toolkit.TableUtils;

/**
 * <p>
 * Entity 对象封装操作类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class FindWrapper<T> extends AbstractWrapper<T, String, FindWrapper<T>> implements Find<FindWrapper<T>, T, String> {
    private static final long serialVersionUID = -6124709475885451709L;

    /**
     * 查询字段
     */
    private final SharedString sqlSelect = new SharedString();

    public FindWrapper() {
        this(null);
    }

    public FindWrapper(T entity) {
        super.setEntity(entity);
        super.initNeed();
    }

    public FindWrapper(T entity, String... columns) {
        super.setEntity(entity);
        super.initNeed();
        this.select(columns);
    }

    /**
     * 非对外公开的构造方法,只用于生产嵌套 sql
     *
     * @param entityClass 本不应该需要的
     */
    private FindWrapper(T entity, Class<T> entityClass, AtomicInteger paramNameSeq,
                         Map<String, Object> paramNameValuePairs, MergeSegments mergeSegments,
                         SharedString lastSql, SharedString sqlComment, SharedString sqlFirst) {
        super.setEntity(entity);
        super.setEntityClass(entityClass);
        this.paramNameSeq = paramNameSeq;
        this.paramNameValuePairs = paramNameValuePairs;
        this.expression = mergeSegments;
        this.lastSql = lastSql;
        this.sqlComment = sqlComment;
        this.sqlFirst = sqlFirst;
    }

    /**
     * <p>
     * 过滤字段
     * 比如："t_sys_user.username", "t_sys_user.password"
     * </p>
     * 
     */
    @Override
    public FindWrapper<T> select(String... columns) {
        return select(getEntityClass(), columns);
    }
    
    /**
     * <p>
     * 过滤字段
     * 比如：clazz = SysUserVo.class, columns="username", "password"
     * </p>
     * 
     */
    @Override
    public FindWrapper<T> select(Class<?> clazz, String... columns) {
        if (ArrayUtils.isNotEmpty(columns)) {
            if (null == clazz) {
                clazz = getEntityClass();
            }
            if (StringUtils.isBlank(this.sqlSelect.getStringValue())) {
                this.sqlSelect.setStringValue(TableUtils.sqlSelect(clazz, columns));
            } else {
                this.sqlSelect.setStringValue(this.sqlSelect.getStringValue() 
                        + Constants.COMMA + TableUtils.sqlSelect(clazz, columns));
            }
        }
        return typedThis;
    }
    
    /**
     * <p>
     * 过滤字段
     * 比如：alias="t_sys_user", columns="username", "password"
     * </p>
     * 
     */
    @Override
    public FindWrapper<T> selectAlias(String alias, String... columns) {
        if (ArrayUtils.isNotEmpty(columns)) {
            if (StringUtils.isBlank(this.sqlSelect.getStringValue())) {
                this.sqlSelect.setStringValue(TableUtils.sqlSelect(alias, columns)); 
            } else {
                this.sqlSelect.setStringValue(this.sqlSelect.getStringValue() 
                        + Constants.COMMA + TableUtils.sqlSelect(alias, columns));
            }
        }
        return typedThis;
    }
    
    @Override
    public FindWrapper<T> selectOrig(String... columns) {
        if (ArrayUtils.isNotEmpty(columns)) {
            this.sqlSelect.setStringValue(String.join(StringPool.COMMA, columns));
        }
        return typedThis;
    }

    @Override
    public FindWrapper<T> selectOrig(Class<T> entityClass, Predicate<TableFieldInfo> predicate) {
        super.setEntityClass(entityClass);
        this.sqlSelect.setStringValue(TableInfoHelper.getTableInfo(getEntityClass()).chooseSelect(predicate));
        return typedThis;
    }

    @Override
    public String getSqlSelect() {
        return sqlSelect.getStringValue();
    }

    /**
     * 返回一个支持 lambda 函数写法的 wrapper
     */
    public LambdaFindWrapper<T> lambda() {
        return new LambdaFindWrapper<>(getEntity(), getEntityClass(), sqlSelect, paramNameSeq, paramNameValuePairs,
            expression, lastSql, sqlComment, sqlFirst);
    }

    /**
     * 用于生成嵌套 sql
     * <p>
     * 故 sqlSelect 不向下传递
     * </p>
     */
    @Override
    protected FindWrapper<T> instance() {
        return new FindWrapper<>(getEntity(), getEntityClass(), paramNameSeq, paramNameValuePairs, new MergeSegments(),
            SharedString.emptyString(), SharedString.emptyString(), SharedString.emptyString());
    }

    @Override
    public void clear() {
        super.clear();
        sqlSelect.toNull();
    }
}
