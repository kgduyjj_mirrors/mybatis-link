/**
 * Project: yui3-common-mybatisx-core
 * Class MybatisxConfiguration
 * Version 1.0
 * File Created at 2020-12-7
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core;

import com.baomidou.mybatisplus.core.MybatisConfiguration;

/**
 * <p>
 * replace default MybatisConfiguration class
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class MybatisLinkConfiguration extends MybatisConfiguration {

    protected final InjectorMapperRegistry injectorMapperRegistry = new InjectorMapperRegistry(this);

    public MybatisLinkConfiguration() {
    	super();
    }
    
    @Override
    public <T> void addMapper(Class<T> type) {
        super.addMapper(type);
        injectorMapperRegistry.addMapper(type);
    }
    
    
    /*@Override
    public Executor newExecutor(Transaction transaction, ExecutorType executorType) {
        executorType = executorType == null ? defaultExecutorType : executorType;
        executorType = executorType == null ? ExecutorType.SIMPLE : executorType;
        Executor executor;
        if (ExecutorType.BATCH == executorType) {
            executor = new MybatisBatchExecutor(this, transaction);
        } else if (ExecutorType.REUSE == executorType) {
            executor = new MybatisReuseExecutor(this, transaction);
        } else {
            executor = new MybatisSimpleExecutor(this, transaction);
        }
        if (cacheEnabled) {
            executor = new MybatisCachingExecutor(executor);
        }
        executor = (Executor) interceptorChain.pluginAll(executor);
        return executor;
    }*/

    
    
}
