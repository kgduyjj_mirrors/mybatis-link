/**
 * Project: yui3-common-mybatisx
 * Class WrapperConstants
 * Version 1.0
 * File Created at 2019年3月25日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit;

/**
 * <p>
 * wrapper 常量
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class MybatisLinkConstants {

    public static final String ID = "id";
    
    public static final String TNT_ID = "tntId";
    
    public static final String SQL_ON_MAP = "sqlOnMap";
    
    public static final String TABLE_NAME = "tableName";
    
    public static final String TABLE_NAME_MAP = "tableNameMap";
}
