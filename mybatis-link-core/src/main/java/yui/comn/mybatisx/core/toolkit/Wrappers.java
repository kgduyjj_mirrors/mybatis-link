/**
 * Project: yui3-common-mybatisx-base
 * Class Wrappers
 * Version 1.0
 * File Created at 2020-12-24
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit;

import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;

/**
 * <p>
 * Wrapper 条件构造
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class Wrappers {

    public static <Y> Wrapper<Y> get(Wrapper<Y> wrapper) {
        if (null == wrapper) {
            return new FindWrapper<>();
        }
        return wrapper;
    }
    
    public static <Y> FindWrapper<Y> get(FindWrapper<Y> wrapper) {
        if (null == wrapper) {
            return new FindWrapper<>();
        }
        return wrapper;
    }
    
}
