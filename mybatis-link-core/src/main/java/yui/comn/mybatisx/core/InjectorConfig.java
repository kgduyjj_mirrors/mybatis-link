/**
* Project: yui3-common-mybatisx
 * Class InjectorConfig
 * Version 1.0
 * File Created at 2019年1月21日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.builder.MapperBuilderAssistant;

import lombok.Data;

/**
 * <p>
 * 注入相关信息
 * <p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class InjectorConfig {

    public static final Map<Class<?>, MapperBuilderAssistant> MAPPER_ASSISTANT = new HashMap<>();
    
    /**
     * 自定义Mapper资源
     */
    public static final Set<String> injectorResources = new HashSet<String>();
    
    public static boolean isInjectorResource(String resource) {
        return injectorResources.contains(resource);
    }
    
    public static void addInjectorResource(String resource) {
        injectorResources.add(resource);
    }
    
}
