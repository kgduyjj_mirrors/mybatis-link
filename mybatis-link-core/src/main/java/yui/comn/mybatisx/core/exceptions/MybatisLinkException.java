package yui.comn.mybatisx.core.exceptions;


/**
 * <p>
 * MybatisPlus 异常类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class MybatisLinkException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MybatisLinkException(String message) {
        super(message);
    }

    public MybatisLinkException(Throwable throwable) {
        super(throwable);
    }

    public MybatisLinkException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
