/**
 * Project: yui3-common-mybatisx
 * Class SoftSqlScriptUtils
 * Version 1.0
 * File Created at 2020年3月12日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.core.toolkit.sql;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

/**
 * <p>
 * sql 脚本工具类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftSqlScriptUtils implements Constants {
    private static final long serialVersionUID = -9211775788645680545L;

    /**
     * <p>
     * 获取 带 foreach 标签的脚本
     * </p>
     *
     * @param sqlScript       sql 脚本片段
     * @param open            以...开头
     * @param close           以...结尾
     * @param separator       分隔符...
     * @return foreach 脚本
     */
    public static String convertForeach(final String sqlScript, final String collection, final String index,
           final String item, final String separator, final String open, final String close) {
        StringBuilder sb = new StringBuilder("<foreach");
        if (StringUtils.isNotBlank(collection)) {
            sb.append(" collection=\"").append(collection).append(QUOTE);
        }
        if (StringUtils.isNotBlank(index)) {
            sb.append(" index=\"").append(index).append(QUOTE);
        }
        if (StringUtils.isNotBlank(item)) {
            sb.append(" item=\"").append(item).append(QUOTE);
        }
        if (StringUtils.isNotBlank(open)) {
            sb.append(" open=\"").append(open).append(QUOTE);
        }
        if (StringUtils.isNotBlank(close)) {
            sb.append(" close=\"").append(close).append(QUOTE);
        }
        if (StringUtils.isNotBlank(separator)) {
            sb.append(" separator=\"").append(separator).append(QUOTE);
        }
        return sb.append(RIGHT_CHEV).append(NEWLINE).append(sqlScript).append(NEWLINE).append("</foreach >").toString();
    }
    
}
