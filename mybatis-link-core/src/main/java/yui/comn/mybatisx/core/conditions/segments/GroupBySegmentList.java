package yui.comn.mybatisx.core.conditions.segments;


import static com.baomidou.mybatisplus.core.enums.SqlKeyword.GROUP_BY;
import static java.util.stream.Collectors.joining;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;

/**
 * <p>
 * Group By SQL 片段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class GroupBySegmentList extends AbstractISegmentList {
    private static final long serialVersionUID = -4827219150739476516L;

    @Override
    protected boolean transformList(List<ISqlSegment> list, ISqlSegment firstSegment, ISqlSegment lastSegment) {
        list.remove(0);
        return true;
    }

    @Override
    protected String childrenSqlSegment() {
        if (isEmpty()) {
            return EMPTY;
        }
        return this.segmentList.stream().map(ISqlSegment::getSqlSegment).collect(joining(COMMA, SPACE + GROUP_BY.getSqlSegment() + SPACE, EMPTY));
    }
}
