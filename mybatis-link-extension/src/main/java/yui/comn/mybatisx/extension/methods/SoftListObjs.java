package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 queryWrapper 条件查询多条数据
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftListObjs extends AbstractSoftMethod {
    private static final long serialVersionUID = -5347007973680358700L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.LIST_OBJS;
        String sql = String.format(sqlMethod.getSql(), sqlFirst(), sqlSelectObjsColumns(tableInfo),
                getDynamicTableName(tableInfo), sqlWhereEntityWrapper(true, tableInfo), sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, Object.class);
    }
}
