package yui.comn.mybatisx.extension.node;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * <p>
 * 节点分装类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class NodeWrapper<T> implements Serializable {
    private static final long serialVersionUID = 5109054894068502210L;

    private List<T> list;
    
    private List<String> checkedKeys = new ArrayList<>();
    
    public void addNode(T node) {
        if (null == list) {
            list = new ArrayList<>();
        }
        list.add(node);
    }
    
    public void addCheckKey(Long key) {
        checkedKeys.add(String.valueOf(key));
    }
    
}
