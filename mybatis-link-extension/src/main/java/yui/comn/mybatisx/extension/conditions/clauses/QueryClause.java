package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class QueryClause implements Serializable {
    private static final long serialVersionUID = -6014076206210293128L;
    
    private List<WhereClause> w;
    private List<OrderClause> o;
    private PageClause p;
    private List<RuleClause> r;
    private List<JoinClause> j;
    private SelectClause s;
    
    private Map<String, WhereClause> ruleKeyWhereMap = new HashMap<>();
    
    /**
     * 给规则条款添加一个查询条款
     */
    public void addRuleWhere(String key, String val) {
    	addRuleWhere(key, val, null, null);
    }
    
    /**
     * 给规则条款添加一个查询条款
     */
    public void addRuleWhere(String key, String val, String mode) {
    	addRuleWhere(key, val, mode, null);
    }
    
    /**
     * 给规则条款添加一个查询条款
     */
    public void addRuleWhere(String key, String val, String mode, String name) {
    	WhereClause wc = new WhereClause();
    	wc.setK(key);
    	wc.setV(val);
    	wc.setM(mode);
    	
    	RuleClause rc = getRuleClause(name);
    	rc.addWhereClause(wc);
    	ruleKeyWhereMap.put(key, wc);
    }
    
    /**
     * 获取规则条款
     */
    public RuleClause getRuleClause(String name) {
    	if (CollectionUtils.isEmpty(r)) {
    		RuleClause rc = new RuleClause();
    		rc.setN(name);
    		addRuleClause(rc);
    		return rc;
    	}
    	
    	if (StringUtils.isBlank(name)) {
    		return r.get(0);
    	}
    	
    	if (StringUtils.isNotBlank(name)) {
    		for (RuleClause rc : r) {
				if (name.equals(rc.getN())) {
					return rc;
				}
			}
    		RuleClause rc = new RuleClause();
    		rc.setN(name);
    		addRuleClause(rc);
    		return rc;
    	}

    	return null;
    }
    
    /**
     * 新增一个规则条款
     */
    public void addRuleClause(RuleClause ruleClause) {
    	if (null == r) {
    		r = new ArrayList<>();
    	}
    	r.add(ruleClause);
    }
    
    /**
     * 通过key，是否包含
     */
    public boolean contain(String key) {
    	return null != getRuleVal(key) ? true : false;
    }
    
    /**
     * 通过key，获取val
     */
    public Object getRuleVal(String key) {
    	return getRuleKvMap().get(key);
    }
    
    /**
     * 获取key val键值对
     */
    public Map<String, Object> getRuleKvMap() {
    	Map<String, Object> ruleKvMap = new HashMap<>();
    	for (Entry<String, WhereClause> entry : ruleKeyWhereMap.entrySet()) {
    		ruleKvMap.put(entry.getKey(), entry.getValue().getV());
		}
    	return ruleKvMap;
    }
    
    /**
     * 初始化 ruleKeyWhereMap
     */
    public void initRuleKeyWhereMap() {
		for (RuleClause ruleClause : r) {
			for (WhereClause wc : ruleClause.getW()) {
				String keyStr = wc.getK();
				// 处理分隔符
                String separatorChars = StringPool.UNDERSCORE + StringPool.UNDERSCORE;
                if (keyStr.contains(StringPool.COMMA)) {
                    separatorChars = StringPool.COMMA;
                }
                String[] keys = keyStr.split(separatorChars);
                for (String key : keys) {
                	ruleKeyWhereMap.put(key, wc);
                }
			}
		}
    }
    
}
