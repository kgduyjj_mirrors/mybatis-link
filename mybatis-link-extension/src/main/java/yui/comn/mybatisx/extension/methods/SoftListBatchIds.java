package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 IDS 查询
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftListBatchIds extends AbstractSoftMethod {
    private static final long serialVersionUID = 8705769185942689062L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.LIST_BATCH_BY_IDS;
        String sql = String.format(sqlMethod.getSql(), sqlSelectAliasColumns(tableInfo, false),
                tableInfo.getTableName(), tableInfo.getKeyColumn(),
                SqlScriptUtils.convertForeach("#{item}", COLLECTION, null, "item", COMMA),
                tableInfo.getLogicDeleteSql(true, true));
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo.getResultMap());
    }
}
