/**
 * Project: yui3-common-mybatisx
 * Class SoftUpdateBatchById
 * Version 1.0
 * File Created at 2020年3月19日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.methods;

import java.util.List;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;
import yui.comn.mybatisx.core.toolkit.SoftConstants;

/**
 * <p>
 * 根据ID 批量修改所有值
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftUpdateBatchById extends AbstractSoftMethod {
    private static final long serialVersionUID = -4754685611319409426L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        /*String sql;
        boolean logicDelete = tableInfo.isLogicDelete();
        SoftSqlMethod sqlMethod = SoftSqlMethod.UPDATE_BATCH_BY_ID;
        StringBuilder append = new StringBuilder("<if test=\"item instanceof java.util.Map\">")
            .append("<if test=\"item.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_ORIGINAL).append("!=null\">")
            .append(" AND ${item.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_COLUMN)
            .append("}=#{item.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_ORIGINAL).append(StringPool.RIGHT_BRACE)
            .append("</if></if>");
        if (logicDelete) {
            append.append(tableInfo.getLogicDeleteSql(true, false));
        }
        
        String sqlSetScript = sqlSet(logicDelete, false, tableInfo, SoftConstants.ITEM, SoftConstants.ITEM_DOT);
        
        String updateScript = String.format(sqlMethod.getSql(), tableInfo.getTableName(),
                sqlSetScript, tableInfo.getKeyColumn(), 
                SoftConstants.ITEM_DOT + tableInfo.getKeyProperty(), append);
        
        sql = String.format(SoftSqlMethod.EMPTY.getSql(), SqlScriptUtils.convertForeach(updateScript, 
                SoftConstants.COLL, SoftConstants.INDEX, SoftConstants.ITEM, SEMICOLON));
        
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addUpdateMappedStatement(mapperClass, List.class, sqlMethod.getMethod(), sqlSource);*/
        
        SoftSqlMethod sqlMethod = SoftSqlMethod.UPDATE_BATCH_BY_ID;
        
        final String additional = optlockVersion(tableInfo) + tableInfo.getLogicDeleteSql(true, true);
        
        String sqlSetScript = sqlSet(tableInfo.isWithLogicDelete(), false, tableInfo, false, SoftConstants.ITEM, SoftConstants.ITEM_DOT);
        
        String updateScript = String.format(sqlMethod.getSql(), tableInfo.getTableName(),
                sqlSetScript, tableInfo.getKeyColumn(), 
                SoftConstants.ITEM_DOT + tableInfo.getKeyProperty(), additional);
        
        String sql = String.format(SoftSqlMethod.EMPTY.getSql(), SqlScriptUtils.convertForeach(updateScript, 
                SoftConstants.COLL, SoftConstants.INDEX, SoftConstants.ITEM, SEMICOLON));
        
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addUpdateMappedStatement(mapperClass, List.class, sqlMethod.getMethod(), sqlSource);
    }

}
