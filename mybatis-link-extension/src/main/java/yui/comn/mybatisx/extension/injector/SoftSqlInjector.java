package yui.comn.mybatisx.extension.injector;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.ibatis.builder.MapperBuilderAssistant;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.AbstractSqlInjector;
import com.baomidou.mybatisplus.core.injector.methods.Delete;
import com.baomidou.mybatisplus.core.injector.methods.DeleteBatchByIds;
import com.baomidou.mybatisplus.core.injector.methods.DeleteById;
import com.baomidou.mybatisplus.core.injector.methods.DeleteByMap;
import com.baomidou.mybatisplus.core.injector.methods.Insert;
import com.baomidou.mybatisplus.core.injector.methods.Update;
import com.baomidou.mybatisplus.core.injector.methods.UpdateById;

import yui.comn.mybatisx.core.InjectorConfig;
import yui.comn.mybatisx.extension.methods.SoftCount;
import yui.comn.mybatisx.extension.methods.SoftDefunct;
import yui.comn.mybatisx.extension.methods.SoftGet;
import yui.comn.mybatisx.extension.methods.SoftGetById;
import yui.comn.mybatisx.extension.methods.SoftGetByMap;
import yui.comn.mybatisx.extension.methods.SoftInsertBatch;
import yui.comn.mybatisx.extension.methods.SoftList;
import yui.comn.mybatisx.extension.methods.SoftListBatchIds;
import yui.comn.mybatisx.extension.methods.SoftListByMap;
import yui.comn.mybatisx.extension.methods.SoftListMaps;
import yui.comn.mybatisx.extension.methods.SoftListMapsByMap;
import yui.comn.mybatisx.extension.methods.SoftListObjs;
import yui.comn.mybatisx.extension.methods.SoftPage;
import yui.comn.mybatisx.extension.methods.SoftPageMaps;
import yui.comn.mybatisx.extension.methods.SoftUpdateAllBatchById;
import yui.comn.mybatisx.extension.methods.SoftUpdateAllById;
import yui.comn.mybatisx.extension.methods.SoftUpdateBatchById;


/**
 * <p>
 * SQL 逻辑删除注入器
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftSqlInjector extends AbstractSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass) {
        return Stream.of(
            new Insert(),
            new SoftInsertBatch(),
            new SoftDefunct(),
            new Delete(),
            new DeleteByMap(),
            new DeleteById(),
            new DeleteBatchByIds(),
            new Update(),
            new UpdateById(),
            new SoftUpdateAllById(),
            new SoftUpdateBatchById(),
            new SoftUpdateAllBatchById(),
            new SoftCount(),
            new SoftGet(),
            new SoftGetById(),
            new SoftGetByMap(),
            new SoftList(),
            new SoftListByMap(),
            new SoftListBatchIds(),
            new SoftListObjs(),
            new SoftListMaps(),
            new SoftListMapsByMap(),
            new SoftPage(),
            new SoftPageMaps()
        ).collect(Collectors.toList());
    }

    @Override
    public void inspectInject(MapperBuilderAssistant builderAssistant, Class<?> mapperClass) {
        super.inspectInject(builderAssistant, mapperClass);
        InjectorConfig.MAPPER_ASSISTANT.put(mapperClass, builderAssistant);
    }
    
    

}
