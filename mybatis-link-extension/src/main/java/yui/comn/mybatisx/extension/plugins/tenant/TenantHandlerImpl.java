///**
// * Project: yui3-common-mybatisx
// * Class YuiTenantHandler
// * Version 1.0
// * File Created at 2019年3月26日
// * $Id$
// * author yuyi
// * email 1060771195@qq.com
// */
//package yui.comn.mybatisx.extension.plugins.tenant;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.baomidou.mybatisplus.extension.plugins.tenant.TenantHandler;
//
//import net.sf.jsqlparser.expression.Expression;
//import net.sf.jsqlparser.expression.LongValue;
//
///**
// * <p>
// * 租户处理器（ TenantId 行级 ）
// * </p>
// *
// * @author yuyi (1060771195@qq.com)
// */
//public class TenantHandlerImpl implements TenantHandler {
//
//    private static final String SYSTEM_TENANT_ID = "tnt_id";
//    private static final List<String> IGNORE_TENANT_TABLES = new ArrayList<>();
//    
//    @Override
//    public Expression getTenantId() {
//        // 从当前系统上下文中取出当前请求的服务商ID，通过解析器注入到SQL中。
//        // MybatisContext.setTntId(0L);
//        Long tntId = MybatisContext.getTntId();
//        if (null == tntId) {
//            return null;
//            // throw new RuntimeException("#1129 getCurrentProviderId error.");
//        }
//        return new LongValue(tntId);
//    }
//
//    @Override
//    public String getTenantIdColumn() {
//        return SYSTEM_TENANT_ID;
//    }
//
//    @Override
//    public boolean doTableFilter(String tableName) {
//     // 忽略掉一些表：如租户表（provider）本身不需要执行这样的处理。
//        return IGNORE_TENANT_TABLES.stream().anyMatch((e) -> e.equalsIgnoreCase(tableName));
//    }
//
//}
