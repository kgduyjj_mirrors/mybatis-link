package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class OrderClause implements Serializable {
    private static final long serialVersionUID = -7864088519623641693L;
    
    private String k; //key
    private String t; //type
    
}
