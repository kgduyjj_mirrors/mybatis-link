package yui.comn.mybatisx.extension.node;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;

/**
 * <p>
 * 节点
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class Node implements Serializable {
    private static final long serialVersionUID = -3514061958234921653L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    
    /**
     * 名称
     */
    private String nm;
    
}
