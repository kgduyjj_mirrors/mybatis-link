/**
 * Project: yui3-common-mybatisx
 * Class SoftUpdateById
 * Version 1.0
 * File Created at 2019年3月15日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 ID 修改所有值
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftUpdateAllById extends AbstractSoftMethod {
    private static final long serialVersionUID = -5824168124268876722L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        /*String sql;
        boolean logicDelete = tableInfo.isLogicDelete();
        SoftSqlMethod sqlMethod = SoftSqlMethod.UPDATE_ALL_BY_ID;
        StringBuilder append = new StringBuilder("<if test=\"et instanceof java.util.Map\">")
            .append("<if test=\"et.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_ORIGINAL).append("!=null\">")
            .append(" AND ${et.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_COLUMN)
            .append("}=#{et.").append(OptimisticLockerInterceptor.MP_OPTLOCK_VERSION_ORIGINAL).append(StringPool.RIGHT_BRACE)
            .append("</if></if>");
        if (logicDelete) {
            append.append(tableInfo.getLogicDeleteSql(true, false));
        }
        sql = String.format(sqlMethod.getSql(), tableInfo.getTableName(),
            sqlAllSet(logicDelete, false, tableInfo, ENTITY, ENTITY_DOT),
            tableInfo.getKeyColumn(), ENTITY_DOT + tableInfo.getKeyProperty(),
            append);
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addUpdateMappedStatement(mapperClass, modelClass, sqlMethod.getMethod(), sqlSource);*/
        
        SoftSqlMethod sqlMethod = SoftSqlMethod.UPDATE_ALL_BY_ID;
        final String additional = optlockVersion(tableInfo) + tableInfo.getLogicDeleteSql(true, true);
        String sql = String.format(sqlMethod.getSql(), tableInfo.getTableName(),
            sqlAllSet(tableInfo.isWithLogicDelete(), false, tableInfo, ENTITY, ENTITY_DOT),
            // sqlSet(tableInfo.isWithLogicDelete(), false, tableInfo, false, ENTITY, ENTITY_DOT),
            tableInfo.getKeyColumn(), ENTITY_DOT + tableInfo.getKeyProperty(), additional);
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addUpdateMappedStatement(mapperClass, modelClass, getMethod(sqlMethod), sqlSource);
    }

}
