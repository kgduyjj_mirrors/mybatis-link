package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 Map 查询
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftGetByMap extends AbstractSoftMethod {
    private static final long serialVersionUID = -4110940614356303891L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.GET_BY_MAP;
        String sql = String.format(sqlMethod.getSql(), sqlSelectAliasColumns(tableInfo, false),
                tableInfo.getTableName(), sqlWhereByMap(tableInfo));
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo.getResultMap());
    }
}
