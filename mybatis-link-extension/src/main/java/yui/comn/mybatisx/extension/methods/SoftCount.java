package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 queryWrapper 条件查询数量
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftCount extends AbstractSoftMethod {
    private static final long serialVersionUID = -2949293557121274583L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.COUNT;
        String sql = String.format(sqlMethod.getSql(), sqlFirst(), sqlCount(), 
                getDynamicTableName(tableInfo), sqlWhereEntityWrapper(true, tableInfo), sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatementForOther(mapperClass, sqlMethod.getMethod(), sqlSource, Integer.class);
    }
}
