/**
* Project: yui3-common-mybatisx
 * Class SoftPage
 * Version 1.0
 * File Created at 2019年1月21日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 queryWrapper 条件查询分页数据
 * </p>
 * 
 * @author yuyi (1060771195@qq.com)
 */
public class SoftPage extends AbstractSoftMethod {
    private static final long serialVersionUID = -3985275088632461911L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.PAGE;
        String sql = String.format(sqlMethod.getSql(), sqlFirst(), sqlSelectAliasColumns(tableInfo, true),
                getDynamicTableName(tableInfo), sqlWhereEntityWrapper(true, tableInfo), sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo.getResultMap());
    }

}
