/**
 * Project: mybatis-link-core
 * Class AbstractCache
 * Version 1.0
 * File Created at 2021-4-18
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.cache;

import java.util.Set;

import org.apache.ibatis.cache.Cache;

import yui.comn.mybatisx.core.cache.CacheMapping;

/**
 * <p>
 * 持久层缓存抽象类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public abstract class AbstractCache implements Cache {

    protected abstract void clear(String id);
    
    protected String getKey(Object key) {
        return key.toString();
    }

    protected String id;
    
    public AbstractCache(String id) {
        super();
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public void clear() {
        clear(id);
        Set<String> ids = CacheMapping.get(id);
        if (null != ids && !ids.isEmpty()) {
        	for (String linkId : ids) {
        		clear(linkId);
        	}
        }
    }

}
