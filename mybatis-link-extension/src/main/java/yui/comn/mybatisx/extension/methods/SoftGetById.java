package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.defaults.RawSqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 ID 查询
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftGetById extends AbstractSoftMethod {
    private static final long serialVersionUID = 1911203133463534353L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.GET_BY_ID;
        String sql = String.format(sqlMethod.getSql(), sqlSelectAliasColumns(tableInfo, false),
                tableInfo.getTableName(), tableInfo.getKeyColumn(), tableInfo.getKeyProperty(),
                tableInfo.getLogicDeleteSql(true, true));
        SqlSource sqlSource = new RawSqlSource(configuration, sql, Object.class);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo.getResultMap());
    }
}
