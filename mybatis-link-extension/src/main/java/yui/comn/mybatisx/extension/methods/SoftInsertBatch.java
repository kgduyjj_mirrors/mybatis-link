/**
 * Project: yui3-common-mybatisx
 * Class InsertBatch
 * Version 1.0
 * File Created at 2020年3月12日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.methods;

import java.util.List;

import org.apache.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;
import yui.comn.mybatisx.core.toolkit.SoftConstants;

/**
 * <p>
 * 批量插入数据
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftInsertBatch extends AbstractSoftMethod {
    private static final long serialVersionUID = -2952131363444007433L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        KeyGenerator keyGenerator = new NoKeyGenerator();
        SoftSqlMethod sqlMethod = SoftSqlMethod.INSERT_BATCH;
        String columnScript = SqlScriptUtils.convertTrim(getAllInsertSqlColumn(tableInfo),
            LEFT_BRACKET, RIGHT_BRACKET, null, COMMA);
        
        String valueScript = SqlScriptUtils.convertTrim(getAllInsertSqlProperty(tableInfo, SoftConstants.ITEM_DOT), 
                LEFT_BRACKET, RIGHT_BRACKET, null, COMMA);
        
        String valuesScript = SqlScriptUtils.convertForeach(valueScript, 
                SoftConstants.COLL, SoftConstants.INDEX, SoftConstants.ITEM, COMMA);
        String keyProperty = null;
        String keyColumn = null;
        // 表包含主键处理逻辑,如果不包含主键当普通字段处理
        if (StringUtils.isNotBlank(tableInfo.getKeyProperty())) {
            if (tableInfo.getIdType() == IdType.AUTO) {
                /** 自增主键 */
                keyGenerator = new Jdbc3KeyGenerator();
                keyProperty = tableInfo.getKeyProperty();
                keyColumn = tableInfo.getKeyColumn();
            } else {
                if (null != tableInfo.getKeySequence()) {
                    keyGenerator = TableInfoHelper.genKeyGenerator(getMethod(sqlMethod), tableInfo, builderAssistant);
                    keyProperty = tableInfo.getKeyProperty();
                    keyColumn = tableInfo.getKeyColumn();
                }
            }
        }
        String sql = String.format(sqlMethod.getSql(), tableInfo.getTableName(), columnScript, valuesScript);
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return this.addInsertMappedStatement(mapperClass, List.class, sqlMethod.getMethod(), sqlSource, keyGenerator, keyProperty, keyColumn);
    }

    /**
     * 获取 insert 时候字段 sql 脚本片段
     * insert into table (字段) values (值)
     * 位于 "字段" 部位
     *
     * @return sql 脚本片段
     */
    private String getAllInsertSqlColumn(TableInfo tableInfo) {
        StringBuffer sqlCol = new StringBuffer(tableInfo.getKeyInsertSqlColumn(false));
        for (TableFieldInfo tfInfo : tableInfo.getFieldList()) {
            // if (!tfInfo.isLogicDelete()) {
            sqlCol.append(NEWLINE).append(tfInfo.getInsertSqlColumn());
            // }
        }
        return sqlCol.toString(); 
    }
    
    /**
     * 获取所有 insert 时候插入值 sql 脚本片段
     * insert into table (字段) values (值)
     * 位于 "值" 部位
     *
     * @return sql 脚本片段
     */
    private String getAllInsertSqlProperty(TableInfo tableInfo, final String prefix) {
        final String newPrefix = prefix == null ? EMPTY : prefix;
        StringBuffer sqlProperty = new StringBuffer(tableInfo.getKeyInsertSqlProperty(newPrefix, false));
        for (TableFieldInfo tfInfo : tableInfo.getFieldList()) {
            // if (!tfInfo.isLogicDelete()) {
            sqlProperty.append(NEWLINE).append(tfInfo.getInsertSqlProperty(newPrefix));
            //}
        }
        return sqlProperty.toString(); 
    }
    
}
