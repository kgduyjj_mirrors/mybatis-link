/**
 * Project: yui3-common-mybatisx
 * Class ApiContext
 * Version 1.0
 * File Created at 2019年3月26日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.plugins.tenant;

/**
 * <p>
 * 租户ID传值
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class MybatisContext {
    public static ThreadLocal<Long> threadLocal = new ThreadLocal<Long>();
    
    public static void setTntId(Long tntId) {
        threadLocal.set(tntId);
    }
    
    public static Long getTntId() {
        return threadLocal.get();
    }
    
    public static void remove() {
    	threadLocal.remove();
    }
}
