package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class WhereClause implements Serializable {
    private static final long serialVersionUID = 5312268117297358992L;
    
    private String k; // key
    private Object v; // val
    private String m; // model
    private String t; // type 'and, or'
    private int s;    // seq
    
}
