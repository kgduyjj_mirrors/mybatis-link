package yui.comn.mybatisx.extension.utils;

import com.google.gson.Gson;

public class GsonUtils {
	
	private static Gson gson = new Gson();
	
	public static <T> T JsonStr2JavaBean(String jsonStr, Class<T> toParseClass){
		if (jsonStr == null) {
			return null;
		}
		return gson.fromJson(jsonStr, toParseClass);
	}
}
