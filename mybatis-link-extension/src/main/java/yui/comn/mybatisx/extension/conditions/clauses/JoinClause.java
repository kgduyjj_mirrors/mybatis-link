package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class JoinClause implements Serializable {
    private static final long serialVersionUID = 2777825099891383775L;
    
    private String n;      // on name
    private List<RuleClause> r; // rule
    
}
