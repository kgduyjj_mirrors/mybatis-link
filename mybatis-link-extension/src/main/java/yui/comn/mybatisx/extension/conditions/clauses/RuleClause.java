package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class RuleClause implements Serializable {
    private static final long serialVersionUID = 5312268117297358992L;
    
    private String n;      // name
    private String t;      // type
    private List<WhereClause> w; // whereClause
    
    public void addWhereClause(WhereClause whereClause) {
    	if (null == w) {
    		w = new ArrayList<>();
    	}
    	w.add(whereClause);
    }
    
}
