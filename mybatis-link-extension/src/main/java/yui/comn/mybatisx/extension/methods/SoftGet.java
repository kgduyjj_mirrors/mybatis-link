package yui.comn.mybatisx.extension.methods;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 queryWrapper 条件查询一条数据
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftGet extends AbstractSoftMethod {
    private static final long serialVersionUID = 5670319127494758021L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.GET;
        String sql = String.format(sqlMethod.getSql(), sqlFirst(), sqlSelectAliasColumns(tableInfo, true),
                getDynamicTableName(tableInfo), sqlWhereEntityWrapper(true, tableInfo), sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass); 
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo.getResultMap());
    }
}
