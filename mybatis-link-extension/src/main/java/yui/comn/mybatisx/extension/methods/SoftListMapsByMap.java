package yui.comn.mybatisx.extension.methods;

import java.util.Map;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 queryWrapper 条件查询多条数据
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftListMapsByMap extends AbstractSoftMethod {
    private static final long serialVersionUID = 3697945170729017262L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.LIST_MAPS_BY_MAP;
        String sql = String.format(sqlMethod.getSql(), sqlSelectColumns(tableInfo, false),
                tableInfo.getTableName(), sqlWhereByMap(tableInfo));
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, Map.class);
    }
}
