package yui.comn.mybatisx.extension.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerializeUtils {
    public static Logger logger = LoggerFactory.getLogger(SerializeUtils.class);
    
	public static byte[] serialize(Object object) {
	    if(null == object){
            return null;
        }
		
		ObjectOutputStream oos = null;
		ByteArrayOutputStream baos = null;
		try {
			// 序列化
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			byte[] bytes = baos.toByteArray();
			return bytes;
		} catch (Exception e) {
		    logger.error("序列化失败", e);
		}
		
		return null;
	}

	public static Object deserialize(byte[] bytes) {
	    if(null == bytes){
            return null;
        }
	    
		ByteArrayInputStream bais = null;
		try {
			// 反序列化
			bais = new ByteArrayInputStream(bytes);
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e) {
		    logger.error("反序列化失败", e);
		}
		return null;
	}
}
