package yui.comn.mybatisx.extension.node;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 树形接口节点
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TreeNode<T> extends Node {
    private static final long serialVersionUID = -8071463452448530550L;

    /**
     * 父id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pid;
    
    /**
     * 排序
     */
    private int seq;
    
    /**
     * 层级
     */
    private int level;
    
    /**
     * 子项集合
     */
    private List<T> children;
    
    public void addChild(T node) {
        if (null == children) {
            children = new ArrayList<>();
        }
        children.add(node);
    }
    
}
