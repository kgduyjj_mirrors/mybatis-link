/**
* Project: yui3-common-mybatisx
 * Class SoftPageMaps
 * Version 1.0
 * File Created at 2019年1月21日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.methods;

import java.util.Map;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

import yui.comn.mybatisx.core.enums.SoftSqlMethod;

/**
 * <p>
 * 根据 Map 查询
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class SoftPageMaps extends AbstractSoftMethod {
    private static final long serialVersionUID = -8475384324951925892L;

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        SoftSqlMethod sqlMethod = SoftSqlMethod.PAGE_MAPS;
        String sql = String.format(sqlMethod.getSql(), sqlFirst(), sqlSelectAliasColumns(tableInfo, true),
                getDynamicTableName(tableInfo), sqlWhereEntityWrapper(true, tableInfo), sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return addSelectMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, Map.class);
    }

}
