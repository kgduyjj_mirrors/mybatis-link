/**
 * Project: yui3-common-mybatisx-base
 * Class FieldClause
 * Version 1.0
 * File Created at 2020-9-28
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.mybatisx.extension.conditions.clauses;

import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * 过滤字段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class SelectClause implements Serializable {
    private static final long serialVersionUID = -6437151833309588671L;

    private Object v; // "id, subId, username" or ["id", "subId", "username"]
    
}
