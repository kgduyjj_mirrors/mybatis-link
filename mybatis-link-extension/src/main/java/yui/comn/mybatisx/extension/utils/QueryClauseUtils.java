package yui.comn.mybatisx.extension.utils;

import yui.comn.mybatisx.extension.conditions.clauses.QueryClause;

public class QueryClauseUtils {

	public static QueryClause JsonStr2JavaBean(String jsonStr){
		if (jsonStr == null) {
			return null;
		}
		QueryClause queryClause = GsonUtils.JsonStr2JavaBean(jsonStr, QueryClause.class);
		queryClause.initRuleKeyWhereMap();
		return queryClause;
	}
}
