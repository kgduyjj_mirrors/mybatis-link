SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_test_a
-- ----------------------------
DROP TABLE IF EXISTS `t_test_a`;
CREATE TABLE `t_test_a`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `c_id` bigint(20) NULL DEFAULT NULL COMMENT 'c主键',
  `b_id` bigint(20) NULL DEFAULT NULL COMMENT 'b主键',
  `a_id` bigint(20) NULL DEFAULT NULL COMMENT 'a主键',
  `rmks` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `crt_tm` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `crt_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `upd_tm` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `upd_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `edit_flag` tinyint(4) NULL DEFAULT 0 COMMENT '逻辑删除',
  `tnt_id` bigint(20) NULL DEFAULT NULL COMMENT '租户主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试A' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_test_b
-- ----------------------------
DROP TABLE IF EXISTS `t_test_b`;
CREATE TABLE `t_test_b`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `a_id` bigint(20) NULL DEFAULT NULL COMMENT 'a主键',
  `c_id` bigint(20) NULL DEFAULT NULL COMMENT 'c主键',
  `crt_tm` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `crt_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `upd_tm` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `upd_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `edit_flag` tinyint(4) NULL DEFAULT 0 COMMENT '逻辑删除',
  `tnt_id` bigint(20) NULL DEFAULT NULL COMMENT '租户主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试B' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_test_c
-- ----------------------------
DROP TABLE IF EXISTS `t_test_c`;
CREATE TABLE `t_test_c`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `a_id` bigint(20) NULL DEFAULT NULL COMMENT 'a主键',
  `b_id` bigint(20) NULL DEFAULT NULL COMMENT 'b主键',
  `c_id` bigint(20) NULL DEFAULT NULL COMMENT 'c主键',
  `crt_tm` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `crt_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `upd_tm` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `upd_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `edit_flag` tinyint(4) NULL DEFAULT 0 COMMENT '逻辑删除',
  `tnt_id` bigint(20) NULL DEFAULT NULL COMMENT '租户主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试C' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_test_d
-- ----------------------------
DROP TABLE IF EXISTS `t_test_d`;
CREATE TABLE `t_test_d`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `a_id` bigint(20) NULL DEFAULT NULL COMMENT 'a主键',
  `crt_tm` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `crt_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `upd_tm` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `upd_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `edit_flag` tinyint(4) NULL DEFAULT 0 COMMENT '逻辑删除',
  `tnt_id` bigint(20) NULL DEFAULT NULL COMMENT '租户主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试E' ROW_FORMAT = Compact;