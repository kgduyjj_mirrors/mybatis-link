package yui.bss.test.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import yui.bss.test.vo.TestAVo;
import yui.bss.test.vo.TestBVo;
import yui.bss.test.vo.TestCVo;
import yui.bss.test.vo.TestDVo;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class TestADto implements Serializable {
    private static final long serialVersionUID = 1L;

    protected TestAVo testAVo2;
    protected TestAVo testAVo;
    protected TestBVo testBVo;
    protected TestCVo testCVo;
    protected TestDVo testDVo;
    protected List<TestBDto> testBList;
    protected List<TestCDto> testCList;
    
    protected Integer count;
    protected String crtBy;

}
