package yui.bss.test.dto;

import java.io.Serializable;

import lombok.Data;
import yui.bss.test.vo.TestCVo;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
public class TestCDto implements Serializable {
    private static final long serialVersionUID = 1L;

    protected TestCVo testCVo;

}
