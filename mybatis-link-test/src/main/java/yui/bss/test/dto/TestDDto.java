package yui.bss.test.dto;

import java.io.Serializable;

import lombok.Data;
import yui.bss.test.vo.TestDVo;

/**
 * <p>
 * 测试E
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
public class TestDDto implements Serializable {
    private static final long serialVersionUID = 1L;

    protected TestDVo testDVo;

}
