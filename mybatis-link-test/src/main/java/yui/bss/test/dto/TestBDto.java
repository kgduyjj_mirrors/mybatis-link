package yui.bss.test.dto;

import java.io.Serializable;

import lombok.Data;
import yui.bss.test.vo.TestBVo;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.annotation.VoDef;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
public class TestBDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @VoDef
    protected TestBVo testBVo;
    protected TestCVo testCVo;

}
