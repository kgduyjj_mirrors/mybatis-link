package yui.bss.test.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import yui.bss.test.dao.TestADao;
import yui.comn.mybatisx.annotation.DaoDef;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2020-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_test_a")
@DaoDef(TestADao.class)
public class TestAVo extends BaseVo {
    private static final long serialVersionUID = 1L;

    // @Note("主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    // @Note("c主键")
    private Long cId;

    // @Note("b主键")
    private Long bId;

    // @Note("a主键")
    private Long aId;

    // @Note("备注")
    private String rmks;

    // @Note("租户主键")
    private Long tntId;



}
