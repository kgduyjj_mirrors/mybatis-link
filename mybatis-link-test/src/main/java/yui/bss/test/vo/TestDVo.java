package yui.bss.test.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 测试E
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_test_d")
public class TestDVo extends BaseVo {
    private static final long serialVersionUID = 1L;

    // @Note("ID")
    private Long id;

    // @Note("aID")
    private Long aId;



}
