package yui.bss.test.vo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;

import lombok.Data;

/**
 * <p>
 * 基础实体类
 * </p>
 * @author yuyi (1060771195@qq.com)
 */
@Data
public abstract class BaseVo implements Serializable {
    private static final long serialVersionUID = 9218209445883907541L;

    @TableField(fill = FieldFill.INSERT)
    private Date crtTm;

    @TableField(fill = FieldFill.INSERT)
    private String crtBy;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    // @JsonFormat(pattern = DateUtils.FULL_ST_FORMAT)
    private Date updTm;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updBy;

    @TableLogic
    private Integer editFlag;
}
