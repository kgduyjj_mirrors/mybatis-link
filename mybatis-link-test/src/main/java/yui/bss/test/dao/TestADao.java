package yui.bss.test.dao;

import java.util.List;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;

import yui.bss.test.dto.TestADto;
import yui.bss.test.dto.TestBDto;
import yui.bss.test.dto.TestCDto;
import yui.bss.test.vo.TestAVo;
import yui.bss.test.vo.TestBVo;
import yui.bss.test.vo.TestCVo;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.annotation.Link;
import yui.comn.mybatisx.annotation.OneToMany;
import yui.comn.mybatisx.annotation.OneToOne;
import yui.comn.mybatisx.annotation.model.JoinType;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.core.mapper.BaseDao;
import yui.comn.mybatisx.extension.cache.RamCache;

/**
 * <p>
 * 测试A Mapper 接口
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Mapper
@CacheNamespace(implementation = RamCache.class, eviction = RamCache.class, flushInterval = 120000)
// @CacheNamespace
public interface TestADao extends BaseDao<TestAVo, TestADto> {

    /**
     * t_test_a 与 t_test_a 一对一连表查询
     * 由于两个表相同，所以必须给其中一个表设置表名，且该别名必须跟TestADto对象中得属性名对应，如：rightAlias = "testAVo2"
     * leftClass默认为TestAVo.class， 如果leftClass为TestAVo.class, 可以默认省略
     * leftColumn或者rightColumn都不写，默认为该表主键
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "a_id", rightClass = TestAVo.class, 
            rightAlias = "testAVo2") })
    List<TestADto> listTestAATestA(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与   t_test_c 一对一连表查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class) })
    List<TestADto> listTestAATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与   t_test_b 一对一连表查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(rightClass = TestBVo.class, rightColumn = "a_id") })
    List<TestADto> listTestAATestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与  t_test_b, t_test_a 与  t_test_c 多表查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class),
                     @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class)})
    List<TestADto> listTestAATestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与  t_test_b, t_test_b 与  t_test_c, t_test_a 与  t_test_d 多表查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class),
                     @OneToOne(leftClass = TestBVo.class, leftColumn = "c_id", rightClass = TestCVo.class),
                     @OneToOne(rightClass = TestDVo.class, rightColumn = "a_id")})
    List<TestADto> listTestAATestBATestCATestD(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 左连接 t_test_b
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.LEFT, onArgName = "abOn") })
    List<TestADto> listTestALtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 左连接 t_test_b, t_test_a 左连接 t_test_c  
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.LEFT, onArgName = "abOn"), 
                     @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class, joinType = JoinType.LEFT, onArgName = "acOn")})
    List<TestADto> listTestALtTestBLtTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 右连接 t_test_c
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class, joinType = JoinType.RIGHT, onArgName = "acOn") })
    // @SqlParser(filter = true)
    List<TestADto> listTestARtTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 右连接 t_test_c 右连接 t_test_b
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class, joinType = JoinType.RIGHT, onArgName = "acOn"),
                     @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.RIGHT, onArgName = "abOn")})
    // @SqlParser(filter = true)
    List<TestADto> listTestARtTestCRtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 内连接 t_test_c 右连接 t_test_b, 多表联查不建议使用右连接
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class),
                     @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.RIGHT, onArgName = "abOn")})
    List<TestADto> listTestAATestCRtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 内连接 t_test_c 左连接 t_test_b
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class),
                    @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.LEFT, onArgName = "abOn")})
    List<TestADto> listTestAATestCLtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 右连接 t_test_b 内连接 t_test_c
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class, joinType = JoinType.LEFT, onArgName = "abOn"),
                    @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class)})
    List<TestADto> listTestALtTestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 一对多 t_test_b
     * 如果t_test_b对应得rightClass 为约定得  TestBVo.class, 可以缺省
     * 由于TestADto中对应得属性List<TestBDto> testBList 这种类型property 对应的是该属性属性名
     */
    @Link(  print = false, printRm = false,
//             manys = { @OneToMany(leftColumn = "b_id", rightClass = TestBVo.class, ofTypeClass = TestBDto.class, property = "testBList") })
            manys = { @OneToMany(leftColumn = "b_id", ofTypeClass = TestBDto.class, property = "testBList") })
    List<TestADto> listTestAWTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
    * t_test_a 一对多 t_test_b, t_test_a 一对多 t_test_c
    */
   @Link(  print = false, printRm = false, 
           manys = { @OneToMany(leftColumn = "b_id", rightClass = TestBVo.class, ofTypeClass = TestBDto.class, property = "testBList"),
                     @OneToMany(leftColumn = "c_id", rightClass = TestCVo.class, ofTypeClass = TestCDto.class, property = "testCList")})
   List<TestADto> listTestAWTestBWTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 一对一  t_test_b, t_test_a 一对多 t_test_b
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class)},
            manys = { @OneToMany(leftClass = TestAVo.class, leftColumn = "b_id", rightClass = TestBVo.class, ofTypeClass = TestBDto.class, property = "testBList") })
    List<TestADto> listTestAATestBWTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 一对一  t_test_b, t_test_a 一对多 t_test_b, 多表中 t_test_b 一对一 t_test_c
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(leftColumn = "b_id", rightClass = TestBVo.class)},
            manys = { @OneToMany(leftClass = TestAVo.class, leftColumn = "b_id", rightClass = TestBVo.class, ofTypeClass = TestBDto.class, property = "testBList", 
            ones = { @OneToOne(leftColumn = "c_id", rightClass = TestCVo.class)} )})
    List<TestADto> listTestAATestBWTestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与   t_test_b 一对一分页连表查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(rightClass = TestBVo.class, rightColumn = "a_id") })
    IPage<TestADto> pageTestAATestB(IPage<TestADto> page, @Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    /**
     * t_test_a 与   t_test_b 连表行数查询
     */
    @Link(  print = false, printRm = false,
            ones = { @OneToOne(rightClass = TestBVo.class, rightColumn = "a_id") })
    Integer countTestAATestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
}
