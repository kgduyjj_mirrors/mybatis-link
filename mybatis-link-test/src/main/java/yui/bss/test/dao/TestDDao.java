package yui.bss.test.dao;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import yui.bss.test.dto.TestDDto;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.core.mapper.BaseDao;
import yui.comn.mybatisx.extension.cache.RamCache;

/**
 * <p>
 * 测试E Mapper 接口
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Mapper
@CacheNamespace(implementation = RamCache.class, flushInterval = 120000)
public interface TestDDao extends BaseDao<TestDVo, TestDDto> {

}
