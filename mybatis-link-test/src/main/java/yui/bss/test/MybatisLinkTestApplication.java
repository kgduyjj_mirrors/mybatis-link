package yui.bss.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yuyi (1060771195@qq.com)
 */
@SpringBootApplication
@ComponentScan("yui")
@MapperScan("yui.bss.*.dao")
public class MybatisLinkTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisLinkTestApplication.class, args);
    }
}
