package yui.bss.test.mgr.impl;

import org.springframework.stereotype.Service;

import yui.bss.test.dao.TestBDao;
import yui.bss.test.dto.TestBDto;
import yui.bss.test.mgr.TestBMgr;
import yui.bss.test.vo.TestBVo;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-04-03
 */
@Service
public class TestBMgrImpl extends MgrImpl<TestBDao, TestBVo, TestBDto> 
        implements TestBMgr {
    
    
}
