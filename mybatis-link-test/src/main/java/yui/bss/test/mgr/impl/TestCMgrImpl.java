package yui.bss.test.mgr.impl;

import org.springframework.stereotype.Service;

import yui.bss.test.dao.TestCDao;
import yui.bss.test.dto.TestCDto;
import yui.bss.test.mgr.TestCMgr;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Service
public class TestCMgrImpl extends MgrImpl<TestCDao, TestCVo, TestCDto> 
        implements TestCMgr {

}
