package yui.bss.test.mgr.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;

import yui.bss.test.dao.TestADao;
import yui.bss.test.dto.TestADto;
import yui.bss.test.mgr.TestAMgr;
import yui.bss.test.vo.TestAVo;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;
import yui.comn.mybatisx.extension.plugins.tenant.MybatisContext;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Service
public class TestAMgrImpl extends MgrImpl<TestADao, TestAVo, TestADto> 
        implements TestAMgr {

    @Override
    public List<TestADto> listTestAATestA(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestA(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestC(Wrapper<TestAVo> wrapper) {
        // FindWrapper<TestAVo> fw = (FindWrapper<TestAVo>) wrapper;
        FindWrapper<TestAVo> fw = (FindWrapper<TestAVo>) wrapper;
        fw.select(TestCVo.class, "a_id", "b_id");
        
        return baseDao.listTestAATestC(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestB(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestB(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestBATestC(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestBATestC(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestBATestCATestD(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestBATestCATestD(wrapper);
    }

    @Override
    public List<TestADto> listTestALtTestB(Wrapper<TestAVo> wrapper) {
        // FindWrapper<TestAVo> fw = (FindWrapper<TestAVo>) wrapper;
        // fw.addSqlOn("abOn", "test");
        List<TestADto> listTestALtTestB = baseDao.listTestALtTestB(wrapper);
        return listTestALtTestB;
    }

    @Override
    public List<TestADto> listTestALtTestBLtTestC(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestALtTestBLtTestC(wrapper);
    }

    @Override
    public List<TestADto> listTestARtTestC(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestARtTestC(wrapper);
    }
    
    @Override
    public List<TestADto> listTestARtTestCRtTestB(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestARtTestCRtTestB(wrapper);
    }
    
    @Override
    public List<TestADto> listTestAATestCRtTestB(Wrapper<TestAVo> wrapper) {
        MybatisContext.setTntId(0L);
        return baseDao.listTestAATestCRtTestB(wrapper);
    }
    
    @Override
    public List<TestADto> listTestAATestCLtTestB(Wrapper<TestAVo> wrapper) {
        MybatisContext.setTntId(0L);
        return baseDao.listTestAATestCLtTestB(wrapper);
    }
    
    @Override
    public List<TestADto> listTestALtTestBATestC(Wrapper<TestAVo> wrapper) {
        MybatisContext.setTntId(0L);
        return baseDao.listTestALtTestBATestC(wrapper);
    }

    @Override
    public List<TestADto> listTestAWTestB(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAWTestB(wrapper);
    }
    
    @Override
    public List<TestADto> listTestAWTestBWTestC(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAWTestBWTestC(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestBWTestB(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestBWTestB(wrapper);
    }

    @Override
    public List<TestADto> listTestAATestBWTestBATestC(Wrapper<TestAVo> wrapper) {
        return baseDao.listTestAATestBWTestBATestC(wrapper);
    }

    @Override
    public IPage<TestADto> pageTestAATestB(Wrapper<TestAVo> wrapper) {
        return baseDao.pageTestAATestB(getPage(wrapper), wrapper);
    }

    @Override
    public Integer countTestAATestB(Wrapper<TestAVo> wrapper) {
        return baseDao.countTestAATestB(wrapper);
    }

}
