package yui.bss.test.mgr;

import yui.bss.test.dto.TestCDto;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
public interface TestCMgr extends IMgr<TestCVo, TestCDto> {

}
