package yui.bss.test.mgr;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;

import yui.bss.test.dto.TestADto;
import yui.bss.test.vo.TestAVo;
import yui.comn.mybatisx.core.conditions.Wrapper;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
public interface TestAMgr extends IMgr<TestAVo, TestADto> {

    List<TestADto> listTestAATestA(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestBATestCATestD(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestALtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestALtTestBLtTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestARtTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestARtTestCRtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestCRtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestCLtTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestALtTestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAWTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAWTestBWTestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestBWTestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    List<TestADto> listTestAATestBWTestBATestC(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    IPage<TestADto> pageTestAATestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
    Integer countTestAATestB(@Param(Constants.WRAPPER) Wrapper<TestAVo> wrapper);
    
}
