package yui.bss.test.mgr.impl;

import org.springframework.stereotype.Service;

import yui.bss.test.dao.TestDDao;
import yui.bss.test.dto.TestDDto;
import yui.bss.test.mgr.TestDMgr;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.extension.mgr.impl.MgrImpl;

/**
 * <p>
 * 测试E
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Service
public class TestDMgrImpl extends MgrImpl<TestDDao, TestDVo, TestDDto> 
        implements TestDMgr {

}
