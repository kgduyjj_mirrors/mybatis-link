package yui.bss.test.mgr;

import yui.bss.test.dto.TestBDto;
import yui.bss.test.vo.TestBVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-04-03
 */
public interface TestBMgr extends IMgr<TestBVo, TestBDto> {

    
}
