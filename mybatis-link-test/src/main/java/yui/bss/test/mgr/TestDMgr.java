package yui.bss.test.mgr;

import yui.bss.test.dto.TestDDto;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试E
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
public interface TestDMgr extends IMgr<TestDVo, TestDDto> {

}
