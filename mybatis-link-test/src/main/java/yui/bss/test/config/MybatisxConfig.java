package yui.bss.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;

import yui.comn.mybatisx.extension.injector.SoftSqlInjector;
import yui.comn.mybatisx.extension.plugins.inner.PaginationCacheInterceptor;

/**
 * mybatis配置
 *
 * @author yuyi (1060771195@qq.com)
 */
@Configuration
public class MybatisxConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationCacheInterceptor(DbType.MYSQL));
        return interceptor;
    }

    
    @Bean
    public ISqlInjector sqlInjector() {
        return new SoftSqlInjector();
    }
    
//  /**
//   * 配置分页 3.0.7.1
//   */
//  @Bean
//  @Order(0)
//  public PaginationInterceptor paginationInterceptor() {
//      PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
//
//      // SQL解析处理拦截：增加租户处理回调。
//      List<ISqlParser> list = new ArrayList<>();
//      list.add(new TenantSqlParser().setTenantHandler(new TenantHandlerImpl()));
//      
//      paginationInterceptor.setSqlParserList(list);
//      return paginationInterceptor;
//  }

}