package yui.bss.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import yui.bss.test.dto.TestBDto;
import yui.bss.test.mgr.TestBMgr;
import yui.bss.test.vo.TestBVo;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-04-01
 */
@Api(value="测试B")
@RestController
@RequestMapping("/test/b")
public class TestBController extends BaseController {
    @Autowired
    private TestBMgr testBMgr;
    
    
    @ApiOperation(value = "分页查询") 
    @GetMapping("page")
    // @RequiresPermissions("test:b:page")
    public Object page(String query) throws Exception { 
        IPage<TestBDto> page = testBMgr.page(getWrapper(query, TestBVo.class));
        return buildPage(page); 
    }
    
    
    @ApiOperation(value = "列表查询") 
    @GetMapping("list")
    // @RequiresPermissions("test:b:list")
    public Object list(String query) { 
        List<TestBDto> list = testBMgr.list(getWrapper(query, TestBVo.class)); 
        return buildList(list); 
    } 
    
    
    @ApiOperation(value = "详情查询")
    @GetMapping("info/{id}")  
    // @RequiresPermissions("test:b:info")
    public Object info(@PathVariable("id") Long id) { 
        TestBDto dto = testBMgr.getById(id);
        return buildObj(dto);
    }
    
    @ApiOperation(value = "删除") 
    @GetMapping("del")
    public Object del(String id) { 
        testBMgr.deleteById(id); 
        return buildSuccess(); 
    } 
    


}