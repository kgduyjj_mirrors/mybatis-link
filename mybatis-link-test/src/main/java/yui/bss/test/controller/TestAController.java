package yui.bss.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import yui.bss.test.dto.TestADto;
import yui.bss.test.mgr.TestAMgr;
import yui.bss.test.vo.TestAVo;
import yui.comn.mybatisx.core.conditions.query.FindWrapper;

/**
 * <p>
 * 测试A
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Api(value="测试A")
@RestController
@RequestMapping("/test/a")
public class TestAController extends BaseController {
    @Autowired
    private TestAMgr testAMgr;
    
    @ApiOperation(value = "分页查询") 
    @GetMapping("page")
    // @RequiresPermissions("test:b:page")
    public Object page(String query) throws Exception { 
        IPage<TestADto> page = testAMgr.page(getWrapper(query, TestAVo.class));
        return buildPage(page); 
    }
    
    
    @ApiOperation(value = "列表查询") 
    @GetMapping("list")
    // @RequiresPermissions("test:b:list")
    public Object list(String query) { 
        List<TestADto> list = testAMgr.list(getWrapper(query, TestAVo.class)); 
        return buildList(list); 
    } 
    
    
    @ApiOperation(value = "详情查询")
    @GetMapping("info/{id}")  
    // @RequiresPermissions("test:b:info")
    public Object info(@PathVariable("id") Long id) { 
        TestADto dto = testAMgr.getById(id);
        return buildObj(dto);
    }
    
    @ApiOperation(value = "删除") 
    @GetMapping("del")
    public Object del(String id) { 
        testAMgr.deleteById(id); 
        return buildSuccess(); 
    } 
    
    // @Log
    @ApiOperation(value = "listTestAATestA") 
    @GetMapping("listTestAATestA")
    public Object listTestAATestA(String query) { 
        FindWrapper<TestAVo> fw = getWrapper(query, TestAVo.class);
        List<TestADto> list = testAMgr.listTestAATestA(fw);
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestC") 
    @GetMapping("listTestAATestC")
    public Object listTestAATestC(String query) { 
        List<TestADto> list = testAMgr.listTestAATestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestB") 
    @GetMapping("listTestAATestB")
    public Object listTestAATestB(String query) { 
        FindWrapper<TestAVo> fw = getWrapper(query, TestAVo.class);
        // fw.addTableName("t_test_a", "t_test_a_ro");
        // fw.addTableName("t_test_b", "t_test_b_ro");
        
        // fw.select(TestAVo.class, "c_id", "crt_tm");
        // fw.select(TestBVo.class, "c_id", "crt_tm");
        // fw.select("t_test_a.c_id", "t_test_a.crt_tm", "t_test_b.c_id", "t_test_b.crt_tm");
        fw.selectAlias("t_test_a", "c_id", "crt_tm");
        fw.selectAlias("t_test_b", "c_id", "crt_tm");
        List<TestADto> list = testAMgr.listTestAATestB(fw);
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestBATestC") 
    @GetMapping("listTestAATestBATestC")
    public Object listTestAATestBATestC(String query) { 
        List<TestADto> list = testAMgr.listTestAATestBATestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestBATestCATestD") 
    @GetMapping("listTestAATestBATestCATestD")
    public Object listTestAATestBATestCATestD(String query) { 
        List<TestADto> list = testAMgr.listTestAATestBATestCATestD(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestALtTestB") 
    @GetMapping("listTestALtTestB")
    public Object listTestALtTestB(String query) { 
        List<TestADto> list = testAMgr.listTestALtTestB(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestALtTestBLtTestC") 
    @GetMapping("listTestALtTestBLtTestC")
    public Object listTestALtTestBLtTestC(String query) { 
        List<TestADto> list = testAMgr.listTestALtTestBLtTestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestARtTestC") 
    @GetMapping("listTestARtTestC")
    public Object listTestARtTestC(String query) { 
        List<TestADto> list = testAMgr.listTestARtTestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestARtTestCRtTestB") 
    @GetMapping("listTestARtTestCRtTestB")
    public Object listTestARtTestCRtTestB(String query) { 
        List<TestADto> list = testAMgr.listTestARtTestCRtTestB(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestCRtTestB") 
    @GetMapping("listTestAATestCRtTestB")
    public Object listTestAATestCRtTestB(String query) { 
        List<TestADto> list = testAMgr.listTestAATestCRtTestB(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestCLtTestB") 
    @GetMapping("listTestAATestCLtTestB")
    public Object listTestAATestCLtTestB(String query) { 
        List<TestADto> list = testAMgr.listTestAATestCLtTestB(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestALtTestBATestC") 
    @GetMapping("listTestALtTestBATestC")
    public Object listTestALtTestBATestC(String query) { 
        List<TestADto> list = testAMgr.listTestALtTestBATestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAWTestB") 
    @GetMapping("listTestAWTestB")
    public Object listTestAWTestB(String query) { 
        List<TestADto> list = testAMgr.listTestAWTestB(getWrapper(query, TestAVo.class));
        return build(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAWTestBWTestC") 
    @GetMapping("listTestAWTestBWTestC")
    public Object listTestAWTestBWTestC(String query) { 
        List<TestADto> list = testAMgr.listTestAWTestBWTestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestBWTestB") 
    @GetMapping("listTestAATestBWTestB")
    public Object listTestAATestBWTestB(String query) { 
        List<TestADto> list = testAMgr.listTestAATestBWTestB(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "listTestAATestBWTestBATestC") 
    @GetMapping("listTestAATestBWTestBATestC")
    public Object listTestAATestBWTestBATestC(String query) { 
        List<TestADto> list = testAMgr.listTestAATestBWTestBATestC(getWrapper(query, TestAVo.class));
        return buildList(list);
    }
    
    // @Log
    @ApiOperation(value = "pageTestAATestB") 
    @GetMapping("pageTestAATestB")
    public Object pageTestAATestB(String query) { 
        IPage<TestADto> page = testAMgr.pageTestAATestB(getWrapper(query, TestAVo.class));
        return buildPage(page);
    }
    
    // @Log
    @ApiOperation(value = "countTestAATestB") 
    @GetMapping("countTestAATestB")
    public Object countTestAATestB(String query) { 
        Integer count = testAMgr.countTestAATestB(getWrapper(query, TestAVo.class));
        return buildObj(count);
    }
    
}